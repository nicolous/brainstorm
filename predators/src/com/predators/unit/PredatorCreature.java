package com.predators.unit;

import java.awt.Color;

import com.predators.game.PredatorsGame;
import com.symugine.game.Game;
import com.symugine.utils.Location;

/**
 * A predator. When a predator reproduces, it gives the half of its life at its child
 */
public class PredatorCreature extends BasePredatorsCreature
{
    public PredatorCreature(final Game game, final Location position, final double radius)
    {
        super(game, position, radius, 100, Color.RED);
    }

    public PredatorCreature(final PredatorCreature p)
    {
        super(p);
    }

    @Override
    public void reproduce()
    {
        final double newHealth = getHealth() / 2;
        setHealth(newHealth);

        final PredatorCreature child = new PredatorCreature(this);
        child.setHealth(newHealth);
        child.setAi(((PredatorsGame) game).getPredatorsAi().newIntelligence());
        game.addObject(child);
    }
}
