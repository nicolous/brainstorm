package com.predators.unit;

import java.awt.Color;

import com.brainstorm.graphics.CircularAppearance;
import com.brainstorm.unit.BrainObject;
import com.predators.game.PredatorsGame;
import com.symugine.game.Game;
import com.symugine.graphics.Animation;
import com.symugine.utils.Location;
import com.symugine.utils.Randomize;

/**
 * Grass are eaten by the preys. They don't have a life span, they grow with time. The more it
 * grows, the more the eater wins life
 */
public class Grass extends BrainObject
{
    /**
     * The appearance is memorized to be dynamically modified when the grass grows
     */
    private final CircularAppearance appearance;

    /** The grow rate */
    private final double growRate;

    /**
     * New grass object
     * 
     * @param game
     *            The game instance
     * @param position
     *            The object's position
     * @param radius
     *            The object's radius
     * @param growRate
     *            The object's grow rate
     */
    public Grass(
            final Game game, final Location position, final double radius, final double growRate)
    {
        super(game, position, radius, 255);
        this.growRate = growRate;

        appearance = new CircularAppearance(new Color(0, 255, 0), true, 0, 0, radius * 2);

        final Animation corpseAnim = new Animation();
        corpseAnim.addFrame(appearance, 1);
        addAnimation(corpseAnim);
    }

    @Override
    public void think(final double elapsed)
    {
        setHealth(getHealth() + growRate * elapsed);

        final double health = getHealth();
        final double maxHealth = getMaxHealth();
        final double healthRatio = health / maxHealth;
        appearance.color = new Color(0, 255 - (int) health, 0);

        if (healthRatio >= 0.75/* && Randomize.random.nextInt(1001) == 1000 */)
        {
            final double radius = getRadius();
            final double diameter = radius * 2;
            final PredatorsGame predGame = (PredatorsGame) game;
            final Location location = getLocation();
            final Location childLocation =
                new Location(
                        location.x - radius - 1 + Randomize.random.nextInt((int) diameter),
                        location.y - radius - 1 + Randomize.random.nextInt((int) diameter));

            final double newHealth = health / 2;
            predGame.addGrass(childLocation, newHealth);
            setHealth(newHealth);
        }

        if (lifeSpan != 0)
        {
            age += elapsed;
            if (age > lifeSpan)
                setHealth(0);
            else
            {
                appearance.color =
                    new Color((int) (200 * age / (double) lifeSpan), appearance.color
                            .getGreen(), 0);
            }
        }
    }
}
