package com.predators.unit;

import java.awt.Color;

import com.brainstorm.graphics.CircularAppearance;
import com.brainstorm.unit.Creature;
import com.symugine.game.Game;
import com.symugine.graphics.Animation;
import com.symugine.utils.Location;

/**
 * Base predators creature. This class is only here to automatically add a grey circle that
 * represent the field of view of the brainstorm creatures
 */
public abstract class BasePredatorsCreature extends Creature
{
    public BasePredatorsCreature(
            final Game game, final Location position, final double radius, final double maxHealth,
            final Color color)
    {
        super(game, position, radius, maxHealth, color);

        init(color);
    }

    public BasePredatorsCreature(final BasePredatorsCreature c)
    {
        super(c);

        init(c.color);
    }

    private void init(final Color color)
    {
        final double radius = getRadius();

        final Animation corpseAnim = new Animation();
        corpseAnim.addFrame(new CircularAppearance(getColor(), true, 0, 0, radius * 2), 1);
        addAnimation(corpseAnim);
    }
}
