package com.predators.unit;

import java.awt.Color;

import com.predators.game.PredatorsGame;
import com.symugine.game.Game;
import com.symugine.utils.Location;

/**
 * A prey When a prey reproduces, it gives the half of its life at its child
 */
public class PreyCreature extends BasePredatorsCreature
{
    public PreyCreature(final Game game, final Location position, final double radius)
    {
        super(game, position, radius, 100, Color.BLUE);
    }

    public PreyCreature(final PreyCreature p)
    {
        super(p);
    }

    @Override
    public void reproduce()
    {
        final double newHealth = getHealth() / 2;
        setHealth(newHealth);

        final PreyCreature child = new PreyCreature(this);
        child.setHealth(newHealth);
        child.setAi(((PredatorsGame) game).getPreysAi().newIntelligence());
        game.addObject(child);
    }
}
