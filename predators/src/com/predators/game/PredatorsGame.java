package com.predators.game;

import com.brainstorm.ai.IntelligenceFactory;
import com.brainstorm.game.BrainGame;
import com.predators.unit.Grass;
import com.predators.unit.PredatorCreature;
import com.predators.unit.PreyCreature;
import com.symugine.game.BaseGameEngine;
import com.symugine.graphics.BaseGraphicsEngine;
import com.symugine.graphics.BaseGui;
import com.symugine.graphics.Scene;
import com.symugine.network.BaseNetworkEngine;
import com.symugine.utils.Location;
import com.symugine.utils.Randomize;
import com.symuscreen.screen.Screen;

/**
 * The predators game oppose two creature teams : preys and predators. Preys eat grass to survive
 * and try to espace the predators. Predators eat preys to survive. All creatures have a limited
 * life span and have to reproduce to perpetrate The last alive team wins.
 */
public class PredatorsGame extends BrainGame
{
    /** Prey AI factory */
    private final IntelligenceFactory preysAi;

    /** Predators AI factory */
    private final IntelligenceFactory predatorsAi;

    /**
     * Prepare a Predators game
     * 
     * @param preysAi
     *            Prey AI factory
     * @param predatorsAi
     *            Predators AI factory
     * @param screen
     *            The game screen
     * @param scene
     *            The game scene
     * @param gameEngine
     *            The game engine
     * @param networkEngine
     *            The network engine
     * @param graphicsEngine
     *            The graphics engine
     * @param gui
     *            The GUI
     */
    public PredatorsGame(
            final IntelligenceFactory preysAi, final IntelligenceFactory predatorsAi,
            final Screen screen, final Scene scene, final BaseGameEngine gameEngine,
            final BaseNetworkEngine networkEngine, final BaseGraphicsEngine graphicsEngine,
            final BaseGui gui)
    {
        super(screen, scene, gameEngine, networkEngine, graphicsEngine, gui);
        this.preysAi = preysAi;
        this.predatorsAi = predatorsAi;

        final int preysCount = 10;
        final int width = scene.getWidth();
        final int height = scene.getHeight();
        int x = 0;
        int y = 0;
        boolean verticalAlign = false;
        switch (Randomize.random.nextInt(3))
        {
        case 0:
            x = Randomize.random.nextInt(width - 10 * preysCount);
            break;
        case 1:
            y = Randomize.random.nextInt(height);
            verticalAlign = true;
            break;
        default:
            break;
        }
        for (int i = 0; i < preysCount; i++)
        {
            if (verticalAlign)
                addPrey(new Location(x, y + i * 10));
            else
                addPrey(new Location(x + i * 10, y));
        }

        for (int i = 0; i < width * height / 100000; i++)
        {
            addPredator(new Location(width / 2, height / 2));
        }

        for (int i = 0; i < width * height / 750; i++)
        {
            addGrass(
                    new Location(Randomize.random.nextInt(width), Randomize.random.nextInt(height)),
                    Randomize.random.nextInt(100) + 1);
        }
    }

    public void addPrey(final Location location)
    {
        final PreyCreature prey1 = new PreyCreature(this, location, 4);
        prey1.setWeakening(0.001);
        prey1.setLifeSpan(5 * 60 * 1000);
        prey1.setStamina(0.03);
        prey1.setView(32);
        prey1.addFood(Grass.class);
        prey1.setAttack(0.05);
        prey1.setRange(5);
        prey1.setAi(preysAi.newIntelligence());
        addObject(prey1);
    }

    public void addPredator(final Location location)
    {
        final PredatorCreature pred1 = new PredatorCreature(this, location, 4);
        pred1.setWeakening(0.001);
        pred1.setLifeSpan(5 * 60 * 1000);
        pred1.setStamina(0.035);
        pred1.setView(48);
        pred1.addFood(PreyCreature.class);
        pred1.setAttack(0.5);
        pred1.setRange(5);
        pred1.setAi(predatorsAi.newIntelligence());
        addObject(pred1);
    }

    public void addGrass(final Location location, final double health)
    {
        final Grass g1 = new Grass(this, location, 4, Randomize.random.nextInt(10) / 10000.0);
        g1.setHealth(health);
        g1.setLifeSpan(10 * 60 * 1000);
        // g1.setAge(Randomize.random.nextInt(10 * 60 * 1000));
        addObject(g1);
    }

    /** Get the preys AI factory */
    public IntelligenceFactory getPreysAi()
    {
        return preysAi;
    }

    /** Get the preydators AI factory */
    public IntelligenceFactory getPredatorsAi()
    {
        return predatorsAi;
    }
}
