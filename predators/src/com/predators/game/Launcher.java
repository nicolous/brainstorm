package com.predators.game;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.UnknownHostException;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import com.brainstorm.ai.IntelligenceFactory;
import com.brainstorm.game.BrainGame;
import com.brainstorm.game.BrainGameEngine;
import com.brainstorm.graphics.BrainGraphicsEngine;
import com.brainstorm.graphics.BrainGui;
import com.brainstorm.graphics.BrainScene;
import com.brainstorm.network.BrainNetworkEngine;
import com.symugine.game.BaseGameEngine;
import com.symugine.graphics.BaseGraphicsEngine;
import com.symugine.graphics.BaseGui;
import com.symugine.graphics.Scene;
import com.symugine.network.BaseNetworkEngine;
import com.symuscreen.screen.Screen;

/**
 * Predators game launcher. Addons that contains an AI factory must have a manifest file with a
 * AIAddon-Class attribute, which defines where is the IntelligenceFactory implemention
 */
public class Launcher
{
    /**
     * Launch a game with the default game screen.
     * 
     * @param preysAi
     *            The preys AI factory
     * @param predatorsAi
     *            The predators AI factory
     * @param fps
     *            Max FPS
     */
    public static void launchGame(
            final IntelligenceFactory preysAi, final IntelligenceFactory predatorsAi,
            final int fps, final int sceneWidth, final int sceneHeight)
    {
        final Screen screen =
                new Screen("Predators", new java.awt.Dimension(400, 400), new java.awt.Dimension(
                        800, 600), false, Color.WHITE);
        launchGame(screen, preysAi, predatorsAi, fps, sceneWidth, sceneHeight);
    }

    /**
     * Launch a game
     * 
     * @param screen
     *            The game screen
     * @param preysAi
     *            The preys AI factory
     * @param predatorsAi
     *            The predators AI factory
     * @param fps
     *            Max FPS
     */
    public static void launchGame(
            final Screen screen, final IntelligenceFactory preysAi,
            final IntelligenceFactory predatorsAi, final int fps, final int sceneWidth,
            final int sceneHeight)
    {
        final Scene scene = new BrainScene(sceneWidth, sceneHeight, screen);
        final BaseGameEngine gameEngine = new PredatorsGameEngine(false);
        final BaseNetworkEngine networkEngine = new BrainNetworkEngine();
        final BaseGraphicsEngine graphicsEngine = new BrainGraphicsEngine(screen);
        final BrainGui gui = new BrainGui(screen);
        final BrainGame game =
                new PredatorsGame(
                        preysAi, predatorsAi, screen, scene, gameEngine, networkEngine,
                        graphicsEngine, gui);
        gui.setGame(game);
        game.setFramerate(fps);
        game.run();
    }

    /**
     * Launch a game and a brainstorm tv server
     * 
     * @param screen
     *            The game screen
     * @param preysAi
     *            The preys AI factory
     * @param predatorsAi
     *            The predators AI factory
     * @param port
     *            The tv server port
     * @param fps
     *            Max FPS
     * @throws UnknownHostException
     * @throws IOException
     */
    public static void launchTv(
            final Screen screen, final IntelligenceFactory preysAi,
            final IntelligenceFactory predatorsAi, final int port, final int fps,
            final int sceneWidth, final int sceneHeight) throws UnknownHostException, IOException
    {
        final Scene scene = new BrainScene(sceneWidth, sceneHeight, screen);
        final BaseGameEngine gameEngine = new BrainGameEngine(false);
        final BrainNetworkEngine networkEngine = new BrainNetworkEngine();
        final BaseGraphicsEngine graphicsEngine = new BrainGraphicsEngine(screen);
        final BaseGui gui = new BaseGui();
        final PredatorsGame game =
                new PredatorsGame(
                        preysAi, predatorsAi, screen, scene, gameEngine, networkEngine,
                        graphicsEngine, gui);
        networkEngine.setBrainstormTv(game, port);
        game.setFramerate(fps);
        game.run();
    }

    /**
     * Load an intelligence factory from a jar file
     * 
     * @param jar
     *            The jar path
     * @return The intelligenceFactory, or null
     * @see Launcher
     */
    private static IntelligenceFactory getAiFactoryFromJar(final String jar)
    {
        IntelligenceFactory addon = null;

        try
        {
            final File file = new File(jar);
            if (!file.exists())
            {
                System.err.println(jar + ": File does not exist");
            }
            else
            {
                final URL[] jarUrl = { file.toURI().toURL() };
                final ClassLoader loader = new URLClassLoader(jarUrl);

                final JarFile jarFile = new JarFile(file);
                final Manifest manifest = jarFile.getManifest();
                if (manifest == null)
                {
                    System.err.println(jar + " has no manifest");
                }
                else
                {
                    final String addonPath = manifest.getMainAttributes().getValue("AiAddon-Class");
                    if (addonPath == null)
                    {
                        System.err.println(jar + ": The manifest has no AiAddon-Class attribute");
                    }
                    else
                    {
                        addon = (IntelligenceFactory) loader.loadClass(addonPath).newInstance();
                    }
                }
            }
        }
        catch (final MalformedURLException e)
        {
            System.err.println(jar + ": Malformed url ");
        }
        catch (final IOException e)
        {
            e.printStackTrace();
            System.err.println(jar + ": Can't read addon.mf");
        }
        catch (final ClassNotFoundException e)
        {
            e.printStackTrace();
            System.err.println("The AiAddon-Class attribute is probably wrong");
        }
        catch (final Exception e)
        {
            e.printStackTrace();
            System.err.println("Can't load " + jar);
        }

        return addon;
    }

    /**
     * Launch a game. <br/>
     * Usage: java -jar predators.jar ai-preys-jar ai-predators-jar [--width scene-width] [--height
     * scene-height] [--tv port [--screen]] [--fps fps-max] --tv port: Broadcast the game on the
     * network (disable render) --screen: Enable render in tv mode
     */
    public static void main(final String[] args)
    {
        boolean success = false;

        if (args.length > 0)
        {
            String preysJar = null;
            String predatorsJar = null;
            boolean isTv = false;
            int port = 0;
            boolean hasScreen = true;
            int fps = 50;
            int sceneWidth = 400;
            int sceneHeight = 400;

            int iarg = 0;
            while (iarg < args.length)
            {
                if (args[iarg].equals("--tv"))
                {
                    if (args.length - iarg > 1)
                    {
                        try
                        {
                            port = Integer.parseInt(args[iarg + 1]);
                            isTv = true;
                            hasScreen = false;
                        }
                        catch (final NumberFormatException e)
                        {
                            System.err.println("Bad port number " + args[iarg + 1]);
                        }
                        iarg += 1;
                    }
                    else
                    {
                        System.err.println("Usage is --tv port");
                    }
                    iarg += 1;
                }
                else if (args[iarg].equals("--screen"))
                {
                    hasScreen = true;
                    iarg += 1;
                }
                else if (args[iarg].equals("--fps"))
                {
                    if (args.length - iarg > 1)
                    {
                        try
                        {
                            fps = Integer.parseInt(args[iarg + 1]);
                        }
                        catch (final NumberFormatException e)
                        {
                            System.err.println("Bad port number " + args[iarg + 1]);
                        }
                        iarg += 1;
                    }
                    else
                    {
                        System.err.println("Usage is --fps maxfps");
                    }
                    iarg += 1;
                }
                else if (args[iarg].equals("--width"))
                {
                    if (args.length - iarg > 1)
                    {
                        try
                        {
                            sceneWidth = Integer.parseInt(args[iarg + 1]);
                        }
                        catch (final NumberFormatException e)
                        {
                            System.err.println("Bad width format " + args[iarg + 1]);
                        }
                        iarg += 1;
                    }
                    else
                    {
                        System.err.println("Usage is --width width");
                    }
                    iarg += 1;
                }
                else if (args[iarg].equals("--height"))
                {
                    if (args.length - iarg > 1)
                    {
                        try
                        {
                            sceneHeight = Integer.parseInt(args[iarg + 1]);
                        }
                        catch (final NumberFormatException e)
                        {
                            System.err.println("Bad height format " + args[iarg + 1]);
                        }
                        iarg += 1;
                    }
                    else
                    {
                        System.err.println("Usage is --height height");
                    }
                    iarg += 1;
                }
                else
                {
                    if (preysJar == null)
                    {
                        preysJar = args[iarg];
                    }
                    else if (predatorsJar == null)
                    {
                        predatorsJar = args[iarg];
                    }
                    else
                    {
                        System.err.println("Bad argument " + args[iarg]);
                    }
                    iarg++;
                }
            }

            if (preysJar != null && predatorsJar != null)
            {
                success = true;

                final IntelligenceFactory preysAddon = getAiFactoryFromJar(preysJar);
                final IntelligenceFactory predatorsAddon = getAiFactoryFromJar(predatorsJar);
                if (preysAddon != null && predatorsAddon != null)
                {
                    if (isTv)
                    {
                        Screen screen = null;
                        if (hasScreen)
                        {
                            screen =
                                    new Screen(
                                            "Predators", new java.awt.Dimension(400, 400),
                                            new java.awt.Dimension(800, 600), false, Color.WHITE);
                        }

                        System.out.println("Launch tv server on port " + port);
                        try
                        {
                            launchTv(
                                    screen, preysAddon, predatorsAddon, port, fps, sceneWidth,
                                    sceneHeight);
                        }
                        catch (final IOException e)
                        {
                            System.err.println(e.getMessage());
                        }
                    }
                    else
                    {
                        launchGame(preysAddon, predatorsAddon, fps, sceneWidth, sceneHeight);
                    }
                }
            }
        }

        if (!success)
        {
            System.out
                    .println("java -jar predators.jar ai-preys-jar ai-predators-jar [--width scene-width] [--height scene-height] [--fps fps-max] [--tv port [--screen]");
        }
    }
}
