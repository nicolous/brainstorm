package com.predators.game;

import com.brainstorm.game.BrainGameEngine;
import com.symugine.graphics.Scene;
import com.symugine.utils.Location;
import com.symugine.utils.Randomize;

public class PredatorsGameEngine extends BrainGameEngine
{
    public PredatorsGameEngine(final boolean passive)
    {
        super(passive);
    }

    @Override
    public void frame(final double elapsed)
    {
        super.frame(elapsed);

        if (Randomize.random.nextInt(10001) == 10000)
        {
            final PredatorsGame predGame = (PredatorsGame) game;
            final Scene scene = game.getScene();
            final int width = scene.getWidth();
            final int height = scene.getHeight();
            for (int i = 0; i < width * height / 100000; i++)
            {
                predGame.addGrass(new Location(Randomize.random.nextInt(width), Randomize.random
                        .nextInt(height)), 1);
            }
        }
    }
}
