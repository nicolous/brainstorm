package com.brainstorm.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import com.symugine.game.Game;
import com.symugine.graphics.Scene;

public class BrainstormTv extends Thread
{
    private class Client extends Thread
    {
        private final Socket connection;
        private final BufferedReader reader;
        private final BufferedWriter writer;

        public Client(final Socket connection) throws IOException
        {
            this.connection = connection;
            reader = new BufferedReader(new InputStreamReader(this.connection.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(this.connection.getOutputStream()));
        }

        public void send(final String message) throws IOException
        {
            writer.write(message + "\n");
            writer.flush();
        }

        public synchronized void disconnect()
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (final IOException e)
                {
                    // e.printStackTrace();
                }
            }

            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (final IOException e)
                {
                    // e.printStackTrace();
                }
            }

            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (final IOException e)
                {
                    // e.printStackTrace();
                }
            }
        }

        public boolean isConnected()
        {
            return connection != null && connection.isConnected();
        }

        @Override
        public void run()
        {
            try
            {
                final Scene scene = game.getScene();
                send(scene.getWidth() + " " + scene.getHeight());

                while (isConnected())
                {
                    reader.readLine();
                    disconnect();
                    removeClient(this);
                }
            }
            catch (final IOException e)
            {
                disconnect();
                removeClient(this);
                // e.printStackTrace();
            }
        }
    }

    private final Game game;

    private Boolean enabled;
    private final ServerSocket server;

    private final List<Client> clients;

    public BrainstormTv(final Game game, final int port) throws IOException
    {
        enabled = true;

        this.game = game;
        server = new ServerSocket(port);
        clients = new LinkedList<Client>();
    }

    public synchronized boolean isEnabled()
    {
        return enabled;
    }

    public synchronized void isEnabled(final boolean enabled)
    {
        this.enabled = enabled;

        if (!this.enabled && server != null)
        {
            try
            {
                server.close();
            }
            catch (final IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public synchronized void addClient(final Client client)
    {
        clients.add(client);
    }

    public synchronized void removeClient(final Client client)
    {
        clients.remove(client);
    }

    @Override
    public void run()
    {
        try
        {
            while (isEnabled())
            {
                Socket connection = null;

                connection = server.accept();
                if (connection != null)
                {
                    Client client = null;
                    try
                    {
                        client = new Client(connection);
                        client.start();
                        addClient(client);
                    }
                    catch (final IOException e)
                    {
                        removeClient(client);
                        e.printStackTrace();
                    }
                }
            }
        }
        catch (final IOException e)
        {
            // e.printStackTrace();
        }
    }

    public synchronized void broadcast(final String message)
    {
        for (final Client client : clients)
        {
            try
            {
                client.send(message);
            }
            catch (final Exception e)
            {
                System.err.println(e.getMessage());
            }
        }
    }
}
