package com.brainstorm.network;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import com.brainstorm.graphics.CircularAppearance;
import com.brainstorm.network.BrainNetworkEngine.PopulationStatsEvent;
import com.brainstorm.network.BrainNetworkEngine.SceneSnapshotEvent;
import com.symugine.graphics.Appearance;
import com.symugine.graphics.Scene.GraphicsData;
import com.symugine.network.BaseNetworkEngine;

public class BrainstormTvClient extends Thread
{
    private Boolean enabled;

    private final BaseNetworkEngine networkEngine;

    private final Socket connection;
    private final BufferedReader reader;
    private final BufferedWriter writer;

    public BrainstormTvClient(final BaseNetworkEngine networkEngine, final String ip, final int port)
            throws UnknownHostException, IOException
    {
        enabled = true;

        this.networkEngine = networkEngine;

        connection = new Socket(ip, port);
        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
    }

    public synchronized boolean isEnabled()
    {
        return enabled;
    }

    public String receive() throws IOException
    {
        return reader.readLine();
    }

    public synchronized void disconnect()
    {
        enabled = false;

        if (reader != null)
        {
            try
            {
                reader.close();
            }
            catch (final IOException e)
            {
                // e.printStackTrace();
            }
        }

        if (writer != null)
        {
            try
            {
                writer.close();
            }
            catch (final IOException e)
            {
                // e.printStackTrace();
            }
        }

        if (connection != null)
        {
            try
            {
                connection.close();
            }
            catch (final IOException e)
            {
                // e.printStackTrace();
            }
        }
    }

    @Override
    public void run()
    {
        try
        {
            while (isEnabled())
            {
                final String message = reader.readLine();
                if (message != null)
                {
                    final String[] data = message.split(" ");
                    try
                    {
                        int cursor = 0;
                        final int nbGraphicsData = Integer.parseInt(data[cursor++]);
                        final ArrayList<GraphicsData> graphicsData =
                                new ArrayList<GraphicsData>(nbGraphicsData);
                        for (int i = 0; i < nbGraphicsData; i++)
                        {
                            final double x = Double.parseDouble(data[cursor++]);
                            final double y = Double.parseDouble(data[cursor++]);
                            final int nbAppearances = Integer.parseInt(data[cursor++]);
                            final ArrayList<Appearance> appearances =
                                    new ArrayList<Appearance>(nbAppearances);
                            for (int j = 0; j < nbAppearances; j++)
                            {
                                final Color color = new Color(Integer.parseInt(data[cursor++]));
                                final double diameter = Double.parseDouble(data[cursor++]);
                                final boolean fill = Boolean.parseBoolean(data[cursor++]);
                                final double offsetx = Double.parseDouble(data[cursor++]);
                                final double offsety = Double.parseDouble(data[cursor++]);
                                appearances.add(new CircularAppearance(
                                        color, fill, offsetx, offsety, diameter));
                            }
                            graphicsData.add(new GraphicsData(x, y, appearances));
                        }
                        final SceneSnapshotEvent snapShotEvent = new SceneSnapshotEvent();
                        snapShotEvent.graphicsData = graphicsData;
                        networkEngine.pushEvent(snapShotEvent);

                        final int nbPopStats = Integer.parseInt(data[cursor++]);
                        final HashMap<String, Integer> populationStats =
                                new HashMap<String, Integer>();
                        for (int i = 0; i < nbPopStats; i++)
                        {
                            populationStats.put(data[cursor++], Integer.parseInt(data[cursor++]));
                        }
                        final PopulationStatsEvent statsEvent = new PopulationStatsEvent();
                        statsEvent.popuplationStats = populationStats;
                        networkEngine.pushEvent(statsEvent);
                    }
                    catch (final Exception e)
                    {
                        disconnect();
                        e.printStackTrace();
                    }
                }
                else
                {
                    disconnect();
                }
            }
        }
        catch (final IOException e)
        {
            // e.printStackTrace();
        }
    }
}
