package com.brainstorm.network;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.brainstorm.game.BrainGameEngine;
import com.brainstorm.graphics.BrainScene;
import com.brainstorm.graphics.CircularAppearance;
import com.symugine.engine.EngineEvent;
import com.symugine.game.Game;
import com.symugine.graphics.Appearance;
import com.symugine.graphics.Scene.GraphicsData;
import com.symugine.network.BaseNetworkEngine;
import com.symugine.utils.Dimension;

public class BrainNetworkEngine extends BaseNetworkEngine
{
    /** Scene snapshot received by the network */
    protected static class SceneSnapshotEvent extends EngineEvent
    {
        /** Graphics data to render */
        public List<GraphicsData> graphicsData;
    }

    /** Population statistics received by the network */
    protected static class PopulationStatsEvent extends EngineEvent
    {
        /** {creature: population} map */
        public Map<String, Integer> popuplationStats;
    }

    /** Networking engine mode */
    public enum Mode
    {
        /** No network stuff (default) */
        NONE,

        /** The engine is client of a brainstorm TV */
        TV_CLIENT,

        /** The engine broadcasts the scene on the network */
        TV_SERVER
    }

    /** Current networking mode */
    protected Mode mode;

    /** TV server thread */
    protected BrainstormTv tvServer;

    /** TV client thread */
    protected BrainstormTvClient tvClient;

    /** New undifferentiated network engine */
    public BrainNetworkEngine()
    {
        mode = Mode.NONE;
    }

    /** Get the current networking mode */
    public Mode getMode()
    {
        return mode;
    }

    /**
     * Make this engine receiving scene snapshots from a brainstorm tv server
     * 
     * @param ip
     *            The TV ip
     * @param port
     *            The TV port
     * @return The scene dimension sent by the TV server
     * @throws UnknownHostException
     *             If the ip address can't be resolved
     * @throws IOException
     *             If the connection is refused
     */
    public Dimension setClientTv(final String ip, final int port)
            throws UnknownHostException, IOException
    {
        Dimension sceneDim = null;

        mode = Mode.TV_CLIENT;
        tvClient = new BrainstormTvClient(this, ip, port);

        final String message = tvClient.receive();
        if (message != null)
        {
            final String[] data = message.split(" ");
            try
            {
                sceneDim = new Dimension(Double.parseDouble(data[0]), Double.parseDouble(data[1]));
            }
            catch (final NumberFormatException e)
            {
                e.printStackTrace();
            }

            tvClient.start();
        }

        return sceneDim;
    }

    /**
     * Make this engine sending scene snapshots to connected network clients
     * 
     * @param game
     *            The game instance
     * @param port
     *            The port on which new connections are listened
     * @throws IOException
     *             If the connection is impossible
     */
    public void setBrainstormTv(final Game game, final int port) throws IOException
    {
        mode = Mode.TV_SERVER;
        tvServer = new BrainstormTv(game, port);
        tvServer.start();
    }

    @Override
    public void frame(final double elapsed)
    {
        switch (mode)
        {
        case TV_CLIENT:
            processEvents();
            break;
        case TV_SERVER:
            final BrainGameEngine gameEngine = (BrainGameEngine) game.getGameEngine();
            final BrainScene scene = (BrainScene) game.getScene();

            final List<GraphicsData> graphicsData = scene.getGraphicsData();
            String message = "";
            message += graphicsData.size();
            for (final GraphicsData data : graphicsData)
            {
                message += " " + data.x + " " + data.y + " " + data.appearances.size();
                for (final Appearance appearance : data.appearances)
                {
                    final CircularAppearance brainAppearance = (CircularAppearance) appearance;
                    message +=
                            " "
                                    + brainAppearance.color.getRGB() + " "
                                    + brainAppearance.diameter + " " + brainAppearance.fill + " "
                                    + brainAppearance.offsetx + " " + brainAppearance.offsety;
                }
            }

            final Map<String, Integer> populationStats = gameEngine.getPopulationStats();
            message += " ";
            message += populationStats.size();
            for (final Entry<String, Integer> gentle : populationStats.entrySet())
            {
                message += " " + gentle.getKey() + " " + gentle.getValue();
            }

            tvServer.broadcast(message);
            break;
        }
    }

    @Override
    protected void processEvent(final EngineEvent event)
    {
        final BrainGameEngine gameEngine = (BrainGameEngine) game.getGameEngine();
        final BrainScene scene = (BrainScene) game.getScene();

        if (event instanceof SceneSnapshotEvent)
        {
            scene.clearGraphicsData();
            final SceneSnapshotEvent snapshotEvent = (SceneSnapshotEvent) event;
            for (final GraphicsData data : snapshotEvent.graphicsData)
            {
                scene.addGraphicsData(data);
            }
        }
        else if (event instanceof PopulationStatsEvent)
        {
            final PopulationStatsEvent statsEvent = (PopulationStatsEvent) event;
            gameEngine.setPopulationStats(statsEvent.popuplationStats);
        }
    }
}
