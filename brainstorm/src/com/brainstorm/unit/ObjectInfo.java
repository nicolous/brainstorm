package com.brainstorm.unit;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.brainstorm.utils.Circle;
import com.symugine.game.Game;
import com.symugine.game.GameObject;
import com.symugine.graphics.Appearance;
import com.symugine.utils.Location;

/** Object information provider */
public class ObjectInfo
{
    /** The game instance */
    protected Game game;

    /** The brainstorm object for which this object provides informations */
    protected BrainObject object;

    /**
     * New information provider
     * 
     * @param game
     *            The game instance
     * @param object
     *            The brainstorm object for which this object provides informations
     */
    public ObjectInfo(final Game game, final BrainObject object)
    {
        this.game = game;
        this.object = object;
    }

    /** Get the object's unique id */
    public long getId()
    {
        return object.getId();
    }

    /** Get the object's [Java] type */
    public Class<?> getObjectType()
    {
        return object.getClass();
    }

    /** Get the object's location */
    public Location getLocation()
    {
        return object.getLocation();
    }

    /** Get the object's stamina */
    public double getStamina()
    {
        return object.getStamina();
    }

    /** Get the object's radius */
    public double getRadius()
    {
        return object.getRadius();
    }

    /** Get the object's health */
    public double getHealth()
    {
        return object.getHealth();
    }

    /** Get the object's max health */
    public double getMaxHealth()
    {
        return object.getMaxHealth();
    }

    /** Get the object's weakening rate */
    public double getWeakening()
    {
        return object.getWeakening();
    }

    /** Get the object's life span (its time to live) */
    public double getLifeSpan()
    {
        return object.getLifeSpan();
    }

    /** Get the object's attack rate */
    public double getAttack()
    {
        return object.getAttack();
    }

    /** Get the object's range (to eat/attack) */
    public double getRange()
    {
        return object.getRange();
    }

    /** Get the object's global appearance */
    public ArrayList<Appearance> getAppearances()
    {
        return object.getAppearances();
    }

    /** Determine if another object can be eaten */
    public boolean isFood(final ObjectInfo infos)
    {
        return object.isFood(infos.object.getClass());
    }

    /** Determine if another object is an enemy */
    public boolean isEnemy(final ObjectInfo infos)
    {
        return object.isEnemy(infos.object.getClass());
    }

    /** Object view [range] */
    public double getView()
    {
        return object.getView();
    }

    /** Object's current age */
    public double getAge()
    {
        return object.age;
    }

    /** Determine if another object is in range (can be eaten/attacked) */
    public boolean isInRange(final ObjectInfo infos)
    {
        return distance(infos) <= object.getRange();
    }

    /**
     * Determine if another object is a pheromone (may be deprecated in the future)
     */
    public boolean isPheromone()
    {
        return object.getClass() == Pheromone.class;
    }

    private final double distance(final Location p1, final Location p2)
    {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    /** Determine the distance between this object and another one */
    public double distance(final ObjectInfo infos)
    {
        return distance(object.getLocation(), infos.object.getLocation());
    }

    /**
     * Determine what objects are in view
     * 
     * @return A list of objects that the creature sees
     */
    public List<ObjectInfo> lookAround()
    {
        final LinkedList<ObjectInfo> objAround = new LinkedList<ObjectInfo>();

        final Location location = object.getLocation();
        final double view = object.getView();
        final long objId = object.getId();
        for (final GameObject otherObj : game.getObjects())
        {
            if (otherObj.getId() != objId)
            {
                final BrainObject otherBrainObj = (BrainObject) otherObj;
                final Circle otherObjCorpse = otherBrainObj.getBoudingBox();
                final double viewRadius = otherObjCorpse.radius + view;
                final Location otherObjPosition = otherBrainObj.getLocation();
                if (distance(location, otherObjPosition) <= viewRadius)
                {
                    objAround.add(otherBrainObj.getObjectInfo());
                }
            }
        }

        return objAround;
    }

    /**
     * Get the pheromone info provider that corresponds to an object
     * 
     * @param object
     *            The object supposed to be a pheromone
     * @return The pheromone infos, or null if the object is not a pheromone
     */
    public PheromoneInfo getPheromone(final ObjectInfo object)
    {
        if (!object.isPheromone())
            throw new IllegalArgumentException("Not a pheromon.");

        final Pheromone ph = (Pheromone) object.object;
        return new PheromoneInfo(ph.getRadius(), ph.getLifeSpan(), ph.getData());
    }
}
