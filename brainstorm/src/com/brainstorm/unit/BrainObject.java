package com.brainstorm.unit;

import java.util.ArrayList;

import com.brainstorm.ai.Actions;
import com.brainstorm.ai.Intelligence;
import com.brainstorm.utils.Circle;
import com.symugine.game.Game;
import com.symugine.game.GameObject;
import com.symugine.utils.Dimension;
import com.symugine.utils.Location;

/**
 * Base brainstorm object. In brainstorm, objects are circular : their dimension correspond to a
 * radius/diameter.
 */
public abstract class BrainObject extends GameObject
{
    /** Object's information provider used by the AI */
    protected ObjectInfo objInfo;

    /** Object's stamina */
    protected double stamina;

    /** Object's health */
    private double health;

    /** Object's maximum health */
    protected double maxHealth;

    /** Object's life span (time to live in ms) */
    protected long lifeSpan;

    /** Object's age (ms) */
    protected long age;

    /** Object's appetite rate */
    protected double weakening;

    /** Objects that this object can eat */
    protected ArrayList<Class<?>> diet;

    /** Object's attack/eat rate */
    protected double attack;

    /** Object's attack/eat range */
    protected double range;

    /** Objects that this object can attack */
    protected ArrayList<Class<?>> enemies;

    /** Object's AI (brain) */
    protected Intelligence ai;

    /** Object's view */
    protected int view;

    protected Actions lastActions;

    public Actions getLastActions()
    {
        return lastActions;
    }

    /**
     * New brain object
     * 
     * @param game
     *            The game instance
     * @param location
     *            The object's location
     * @param radius
     *            The object's radius
     * @param maxHealth
     */
    public BrainObject(
            final Game game, final Location location, final double radius, final double maxHealth)
    {
        super(game, location, new Dimension(radius * 2, radius * 2));
        objInfo = new ObjectInfo(game, this);
        health = maxHealth;
        this.maxHealth = maxHealth;
        diet = new ArrayList<Class<?>>();
        enemies = new ArrayList<Class<?>>();
        view = 0;
        age = 0;
    }

    @SuppressWarnings("unchecked")
    public BrainObject(final BrainObject b)
    {
        super(b);
        objInfo = new ObjectInfo(game, this);
        stamina = b.stamina;

        health = b.health;
        maxHealth = b.maxHealth;
        lifeSpan = b.lifeSpan;
        weakening = b.weakening;

        attack = b.attack;
        range = b.range;

        diet = (ArrayList<Class<?>>) b.diet.clone();
        enemies = (ArrayList<Class<?>>) b.enemies.clone();

        ai = null;
        view = b.view;
        age = 0;
    }

    /** Get the object's info provider */
    public ObjectInfo getObjectInfo()
    {
        return objInfo;
    }

    /** Get the object's bounding box */
    public Circle getBoudingBox()
    {
        final Location location = getLocation();
        return new Circle((int) location.x, (int) location.y, getRadius());
    }

    /** Get the object's radius */
    public double getRadius()
    {
        return dimension.width / 2;
    }

    /** Get the object's stamina */
    public double getStamina()
    {
        return stamina;
    }

    /** Get the object's stamina */
    public void setStamina(final double stamina)
    {
        this.stamina = stamina;
    }

    /** Get the object's health */
    public double getHealth()
    {
        return health;
    }

    /** Set the object's health */
    public void setHealth(final double health)
    {
        this.health = Math.max(0, Math.min(health, maxHealth));
    }

    /** Get the object's maximum health */
    public double getMaxHealth()
    {
        return maxHealth;
    }

    /** Get the object's life span (time to live in ms) */
    public long getLifeSpan()
    {
        return lifeSpan;
    }

    /** Set the object's life span (time to live in ms) */
    public void setLifeSpan(final long lifeSpan)
    {
        this.lifeSpan = lifeSpan;
    }

    /** Get the object's weakening rate */
    public double getWeakening()
    {
        return weakening;
    }

    /** Set the object's weakening rate */
    public void setWeakening(final double weakening)
    {
        this.weakening = weakening;
    }

    /** Get the object's attack/eat rate */
    public double getAttack()
    {
        return attack;
    }

    /** Set the object's attack/eat rate */
    public void setAttack(final double attack)
    {
        this.attack = attack;
    }

    /** Get the object's attack/eat range */
    public double getRange()
    {
        return range;
    }

    /** Set the object's attack/eat range */
    public void setRange(final double range)
    {
        this.range = range;
    }

    /** Add a type of object that this object can eat */
    public void addFood(final Class<?> type)
    {
        diet.add(type);
    }

    /** Determine if a certain type of object can be eaten by this object */
    public boolean isFood(final Class<?> type)
    {
        return diet.contains(type);
    }

    /** Add a type of object that this object can attack */
    public void addEnemy(final Class<?> type)
    {
        enemies.add(type);
    }

    /** Determine if a certain type of object can be attacked by this object */
    public boolean isEnemy(final Class<?> type)
    {
        return enemies.contains(type);
    }

    /** Set the AI of this object */
    public void setAi(final Intelligence ai)
    {
        this.ai = ai;
    }

    /** Get the object's view */
    public int getView()
    {
        return view;
    }

    /** Set the object's view */
    public void setView(final int view)
    {
        this.view = view;
    }

    /** Get the object's current age */
    public long getAge()
    {
        return age;
    }

    /** Set the object's current age */
    public void setAge(final long age)
    {
        this.age = age;
    }

    /**
     * Move this object horizontally
     * 
     * @param initialStamina
     *            The stamina before that the object moves
     * @param destination
     *            Destination to reach if the object has stamina enough
     * @return The stamina after that the object moves
     */
    public double moveX(double initialStamina, final Location destination)
    {
        final Location location = getLocation();
        double distance = 0;
        if (location.x < destination.x)
        {
            distance = Math.min(destination.x - location.x, initialStamina);
            location.x += distance;
        }
        else if (location.x > destination.x)
        {
            distance = Math.min(location.x - destination.x, initialStamina);
            location.x -= distance;
        }
        setLocation(location);

        initialStamina -= distance;
        return initialStamina;
    }

    /**
     * Move this object vertically
     * 
     * @param initialStamina
     *            The stamina before that the object moves
     * @param destination
     *            Destination to reach if the object has stamina enough
     * @return The stamina after that the object moves
     */
    public double moveY(double initialSpeed, final Location destination)
    {
        final Location location = getLocation();
        double distance = 0;
        if (location.y < destination.y)
        {
            distance = Math.min(destination.y - location.y, initialSpeed);
            location.y += distance;
        }
        else if (location.y > destination.y)
        {
            distance = Math.min(location.y - destination.y, initialSpeed);
            location.y -= distance;
        }
        setLocation(location);

        initialSpeed -= distance;
        return initialSpeed;
    }
}
