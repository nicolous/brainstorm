package com.brainstorm.unit;

import java.awt.Color;

import com.brainstorm.graphics.CircularAppearance;
import com.symugine.game.Game;
import com.symugine.graphics.Animation;
import com.symugine.utils.Location;

/**
 * A pheromone is the communication medium between AI. They can be assimilated to a sound, a sign, a
 * smell, etc.
 */
public class Pheromone extends BrainObject
{
    /** Meaning of this pheromone */
    private final long data;

    /**
     * New pheromone
     * 
     * @param game
     *            The game instance
     * @param location
     *            The pheromone's location
     * @param radius
     *            The pheromone's radius
     * @param lifeSpan
     *            The pheromone's life span (time to live in ms)
     * @param data
     *            The pheromone's meaning
     */
    public Pheromone(
            final Game game, final Location location, final double radius, final long lifeSpan,
            final long data)
    {
        super(game, location, radius, 1);
        setLifeSpan(lifeSpan);
        this.data = data;

        final Color color = new Color((int) data);

        final Animation corpseAnim = new Animation();
        corpseAnim.addFrame(new CircularAppearance(color, true, 0, 0, 2), 1);
        addAnimation(corpseAnim);

        // final Animation echoAnim = new Animation();
        // echoAnim.addFrame(
        // new CircularAppearance(color, false, -radius / 2, -radius / 2, radius),
        // lifeSpan / 4);
        // echoAnim.addFrame(new CircularAppearance(
        // color, false, -radius / 1.30, -radius / 1.30, radius / 2),
        // lifeSpan / 4);
        // addAnimation(echoAnim);

    }

    @Override
    public void think(final double elapsed)
    {
        if (lifeSpan != 0)
        {
            age += elapsed;
            if (age > lifeSpan)
            {
                setHealth(0);
            }
        }
    }

    /** Get the pheromone meaning */
    public long getData()
    {
        return data;
    }
}
