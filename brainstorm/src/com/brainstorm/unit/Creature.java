package com.brainstorm.unit;

import java.awt.Color;

import com.symugine.game.Game;
import com.symugine.utils.Location;

/**
 * Creature uses the AI and controls what it does (so the AI can't cheat). A Creature also has the
 * ability to reproduce itself.
 */
public abstract class Creature extends BrainObject
{
    protected Color color;

    public Creature(
            final Game game, final Location location, final double radius, final double maxHealth,
            final Color color)
    {
        super(game, location, radius, maxHealth);

        this.color = color;
    }

    public Creature(final Creature c)
    {
        super(c);
        color = c.color;
    }

    public void setColor(final Color color)
    {
        this.color = color;
    }

    public Color getColor()
    {
        return color;
    }

    @Override
    public void think(final double elapsed)
    {
        if (ai != null)
        {
            lastActions = ai.think(elapsed, objInfo);

            setHealth(getHealth() - weakening * elapsed);

            if (lifeSpan != 0)
            {
                age += elapsed;
                if (age > lifeSpan)
                    setHealth(0);
            }
        }
    }

    public abstract void reproduce();
}
