package com.brainstorm.unit;

/** Pheromone information provider */
public class PheromoneInfo
{
    /** The pheromone's radius */
    public double radius;

    /** The pheromone's life span (time to live) */
    public long lifeSpan;

    /** The pheromone's meaning */
    public long data;

    /**
     * Create a pheromone
     * 
     * @param radius
     *            The pheromone's radius
     * @param lifeSpan
     *            The pheromone's life span
     * @param data
     *            The pheromone's meaning
     */
    public PheromoneInfo(final double radius, final long lifeSpan, final long data)
    {
        this.radius = radius;
        this.lifeSpan = lifeSpan;
        this.data = data;
    }
}
