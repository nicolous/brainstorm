package com.brainstorm.utils;

public class Circle
{
    public double x;
    public double y;
    public double radius;

    public Circle(final double x, final double y, final double rayon)
    {
        this.x = x;
        this.y = y;
        radius = rayon;
    }
}
