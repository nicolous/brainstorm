package com.brainstorm.game;

import com.symugine.game.BaseGameEngine;
import com.symugine.game.Game;
import com.symugine.graphics.BaseGraphicsEngine;
import com.symugine.graphics.BaseGui;
import com.symugine.graphics.Scene;
import com.symugine.network.BaseNetworkEngine;
import com.symugine.sound.BaseSoundEngine;
import com.symuscreen.screen.Screen;

/** Brain game class */
public class BrainGame extends Game
{
    public BrainGame(
            final Screen screen, final Scene scene, final BaseGameEngine gameEngine,
            final BaseNetworkEngine networkEngine, final BaseGraphicsEngine graphicsEngine,
            final BaseGui gui)
    {
        super(gameEngine, networkEngine, graphicsEngine, new BaseSoundEngine(), gui, scene);
    }
}
