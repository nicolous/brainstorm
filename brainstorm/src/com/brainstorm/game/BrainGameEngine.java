package com.brainstorm.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.brainstorm.ai.Actions;
import com.brainstorm.graphics.BrainScene;
import com.brainstorm.unit.BrainObject;
import com.brainstorm.unit.Creature;
import com.brainstorm.unit.ObjectInfo;
import com.brainstorm.unit.Pheromone;
import com.symugine.game.BaseGameEngine;
import com.symugine.game.GameObject;
import com.symugine.graphics.Appearance;
import com.symugine.graphics.Scene.GraphicsData;
import com.symugine.utils.Location;

/** Brainstorm game engine */
public class BrainGameEngine extends BaseGameEngine
{
    /** If the engine is passive (don't update the objects) */
    private final boolean passive;

    /** Game population */
    private Map<String, Integer> populationStats;

    /**
     * New brain game engine
     * 
     * @param passive
     *            If the engine is passive (don't update the objects)
     */
    public BrainGameEngine(final boolean passive)
    {
        this.passive = passive;

        populationStats = new HashMap<String, Integer>();
    }

    public Map<String, Integer> getPopulationStats()
    {
        return populationStats;
    }

    public void setPopulationStats(final Map<String, Integer> populationStats)
    {
        this.populationStats = populationStats;
    }

    @Override
    public void frame(final double elapsed)
    {
        if (!passive)
        {
            final BrainScene scene = (BrainScene) game.getScene();
            final List<GameObject> objects = game.getObjects();

            scene.clearGraphicsData();
            for (final String gentle : populationStats.keySet())
            {
                populationStats.put(gentle, 0);
            }

            final ExecutorService executor =
                    Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            for (final GameObject obj : objects)
            {
                final BrainObject object = (BrainObject) obj;
                if (object.getHealth() > 0)
                {
                    executor.execute(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            object.think(elapsed);
                        }
                    });
                }
                else
                {
                    game.removeObject(object);
                }

                final String objClass = object.getClass().getSimpleName();
                if (!populationStats.containsKey(objClass))
                {
                    populationStats.put(objClass, 1);
                }
                else
                {
                    populationStats.put(objClass, populationStats.get(objClass) + 1);
                }
            }

            executor.shutdown();
            try
            {
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            }
            catch (final InterruptedException e)
            {
                e.printStackTrace();
            }

            for (final GameObject obj : objects)
            {
                final BrainObject object = (BrainObject) obj;
                final Actions actions = object.getLastActions();
                if (actions != null)
                {
                    if (actions.movement != null)
                    {
                        final double initialSpeed =
                                Math.max(Math.min(actions.movement.speed, object.getStamina()), 0)
                                * elapsed;
                        // System.out.println(initialSpeed);
                        // final Location location = getLocation();
                        // final double distanceX = Math.abs(actions.movement.location.x -
                        // location.x);
                        // final double distanceY = Math.abs(actions.movement.location.y -
                        // location.y);
                        // if (distanceX > distanceY)
                        // {
                        // initialSpeed = moveX(initialSpeed, actions.movement.location);
                        // initialSpeed = moveY(initialSpeed, actions.movement.location);
                        // }
                        // else
                        // {
                        // initialSpeed = moveY(initialSpeed, actions.movement.location);
                        // initialSpeed = moveX(initialSpeed, actions.movement.location);
                        // }
                        object.moveX(initialSpeed, actions.movement.location);
                        object.moveY(initialSpeed, actions.movement.location);
                    }
                    else if (actions.toEat != null)
                    {
                        final ObjectInfo objInfo = object.getObjectInfo();
                        if (objInfo.distance(actions.toEat) <= object.getRange())
                        {
                            if (objInfo.isFood(actions.toEat))
                            {
                                final BrainObject target =
                                        (BrainObject) game.findObject(actions.toEat.getId());
                                final double targetOriginalHealth = target.getHealth();
                                target.setHealth(targetOriginalHealth
                                        - object.getAttack() * elapsed);
                                object.setHealth(object.getHealth()
                                        + targetOriginalHealth - target.getHealth());
                            }
                        }
                    }
                    else if (actions.toAttack != null)
                    {
                        final ObjectInfo objInfo = object.getObjectInfo();
                        if (objInfo.distance(actions.toAttack) <= object.getRange())
                        {
                            if (objInfo.isEnemy(actions.toAttack))
                            {
                                final BrainObject target =
                                        (BrainObject) game.findObject(actions.toAttack.getId());
                                target.setHealth(target.getHealth() - object.getAttack() * elapsed);
                            }
                        }
                    }
                    else if (actions.reproducing)
                    {
                        if (object instanceof Creature)
                            ((Creature) object).reproduce();
                        else
                            System.err.println("Seules les cr�atures peuvent se reproduire.");

                    }
                    else if (actions.pheromoneToDrop != null)
                    {
                        game.addObject(new Pheromone(
                                game, object.getLocation(), actions.pheromoneToDrop.radius,
                                actions.pheromoneToDrop.lifeSpan, actions.pheromoneToDrop.data));
                    }
                }

                final Location location = object.getLocation();
                final double radius = object.getRadius();
                object.updateAnimations(elapsed);

                final ArrayList<Appearance> appearances = object.getAppearances();
                if (!appearances.isEmpty())
                {
                    scene.addGraphicsData(new GraphicsData(location.x - radius, location.y
                            - radius, appearances));
                }
            }
        }
    }
}
