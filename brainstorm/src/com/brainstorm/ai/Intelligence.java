package com.brainstorm.ai;

import com.brainstorm.unit.ObjectInfo;

/** Base AI class */
public abstract class Intelligence
{
    /**
     * Invoked each game frame to keep the AI aware of the game
     * 
     * @param elapsed
     *            Time elapsed since the last call
     * @param me
     *            Informations about the creature associated to this AI
     * @return Actions that the creature wants to do
     */
    public abstract Actions think(double elapsed, ObjectInfo me);
}
