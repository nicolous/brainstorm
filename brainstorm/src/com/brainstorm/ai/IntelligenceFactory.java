package com.brainstorm.ai;

/** Intelligence factory */
public interface IntelligenceFactory
{
    /**
     * Called by the engine
     * 
     * @return Must return a new Intelligence instance that will be affected to a creature
     */
    public Intelligence newIntelligence();
}
