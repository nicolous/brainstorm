package com.brainstorm.ai;

import com.brainstorm.unit.ObjectInfo;
import com.brainstorm.unit.PheromoneInfo;
import com.symugine.utils.Movement;

/**
 * Actions that a Creature can do. Only one action can be done at the same time
 */
public class Actions
{
    /** Movement to do */
    public Movement movement;

    /** Object to eat */
    public ObjectInfo toEat;

    /** Object to attack */
    public ObjectInfo toAttack;

    /** Reproduce */
    public boolean reproducing;

    /** Pheromone to drop */
    public PheromoneInfo pheromoneToDrop;

    public Actions()
    {
        reproducing = false;
    }

    /**
     * Move this Creature
     * 
     * @param movement
     *            Movement description
     */
    public void move(final Movement movement)
    {
        this.movement = movement;
    }

    /**
     * Eat an object
     * 
     * @param toEat
     *            Object to eat
     */
    public void eat(final ObjectInfo toEat)
    {
        this.toEat = toEat;
    }

    /**
     * Attack an object
     * 
     * @param toAttack
     *            Object to attack
     */
    public void attack(final ObjectInfo toAttack)
    {
        this.toAttack = toAttack;
    }

    /** Reproduce this Creature */
    public void reproduce()
    {
        reproducing = true;
    }

    /**
     * Drop a pheromone (message)
     * 
     * @param toDrop
     *            Pheromone description
     */
    public void dropPheromone(final PheromoneInfo toDrop)
    {
        pheromoneToDrop = toDrop;
    }
}
