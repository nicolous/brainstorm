package com.brainstorm.graphics;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import com.brainstorm.game.BrainGame;
import com.brainstorm.unit.Creature;
import com.symugine.game.GameObject;
import com.symugine.graphics.BaseGui;
import com.symugine.graphics.Scene;
import com.symugine.utils.Location;
import com.symuscreen.screen.Screen;

public class BrainGui extends BaseGui
{
    private final Screen screen;
    private BrainGame brainGame;

    public BrainGui(final Screen screen)
    {
        this.screen = screen;
    }

    public void setGame(final BrainGame game)
    {
        brainGame = game;
        screen.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(final MouseEvent e)
            {
                final Scene scene = brainGame.getScene();
                final Point click = screen.getMousePosition();

                boolean change = false;

                final List<GameObject> objects = brainGame.getObjects();
                for (final GameObject object : objects)
                {
                    if (object instanceof Creature)
                    {
                        final Creature creature = (Creature) object;
                        final Location location = creature.getLocation();
                        final double radius = creature.getRadius();
                        final int objX = (int) location.x;
                        final int objY = (int) location.y;
                        final int objWidth = (int) radius * 2;
                        if (new Rectangle(objX, objY, objWidth, objWidth).contains(click))
                        {
                            scene.addSelection(object);
                            change = true;
                        }
                    }
                }

                if (!change)
                    scene.getSelection().clear();
            }
        });
    }
}
