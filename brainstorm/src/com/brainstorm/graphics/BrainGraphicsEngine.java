package com.brainstorm.graphics;

import com.symugine.graphics.BaseGraphicsEngine;
import com.symuscreen.screen.Screen;

/** Brainstorm graphics engine */
public class BrainGraphicsEngine extends BaseGraphicsEngine
{
    /** The game screen */
    private final Screen screen;

    /**
     * New brainstorm graphics engine
     * 
     * @param screen
     *            The game screen
     */
    public BrainGraphicsEngine(final Screen screen)
    {
        super(screen == null);
        this.screen = screen;
    }

    /** Get the game screen */
    public Screen getScreen()
    {
        return screen;
    }

    /*
     * @Override public void frame(final double elapsed) { super.frame(elapsed); }
     */
}
