package com.brainstorm.graphics;

import java.awt.Color;

import com.symuscreen.screen.Screen;

/** Brainstorm game circular objects appearance */
public class CircularAppearance extends BrainAppearance
{
    /** The object's diameter */
    public double diameter;

    /**
     * New appearance
     * 
     * @param diameter
     *            The object's diameter
     * @see BrainAppearance
     */
    public CircularAppearance(
            final Color color, final boolean fill, final double offsetx, final double offsety,
            final double diameter)
    {
        super(color, fill, offsetx, offsety);
        this.diameter = diameter;
    }

    @Override
    public void draw(final Screen screen, final double x, final double y, final double zoom)
    {
        final int virtualDiameter = (int) Math.max(diameter * zoom, 2);
        screen.drawOval(color, (int) x, (int) y, virtualDiameter, virtualDiameter, fill);
    }

    @Override
    public double getWidth()
    {
        return diameter;
    }

    @Override
    public double getHeight()
    {
        return diameter;
    }
}
