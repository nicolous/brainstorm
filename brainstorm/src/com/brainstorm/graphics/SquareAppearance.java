package com.brainstorm.graphics;

import java.awt.Color;

import com.symuscreen.screen.Screen;

/** Brainstorm game square objects appearance */
public class SquareAppearance extends BrainAppearance
{
    public double width;

    public double height;

    public SquareAppearance(
            final Color color, final boolean fill, final double offsetx, final double offsety,
            final double with, final double height)
    {
        super(color, fill, offsetx, offsety);
        width = with;
        this.height = height;
    }

    @Override
    public void draw(final Screen screen, final double x, final double y, final double zoom)
    {
        final int virtualWidth = (int) Math.max(width * zoom, 2);
        final int virtualHeight = (int) Math.max(height * zoom, 2);
        screen.drawRect(color, (int) x, (int) y, virtualWidth, virtualHeight, fill);
    }

    @Override
    public double getWidth()
    {
        return height;
    }

    @Override
    public double getHeight()
    {
        return width;
    }
}
