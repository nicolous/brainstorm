package com.brainstorm.graphics;

import java.awt.Color;

import com.symugine.graphics.Appearance;
import com.symuscreen.screen.Screen;

public abstract class BrainAppearance implements Appearance
{
    /** The object's color */
    public Color color;

    /** If the object is filled */
    public boolean fill;

    /**
     * Offset to apply to the object's x-coordinate when rendering this appearance
     */
    public double offsetx;

    /**
     * Offset to apply to the object's y-coordinate when rendering this appearance
     */
    public double offsety;

    /**
     * New appearance
     * 
     * @param color
     *            The object's color
     * @param fill
     *            If the object is filled
     * @param offsetx
     *            Offset to apply to the object's x-coordinate when rendering this appearance
     * @param offsety
     *            Offset to apply to the object's y-coordinate when rendering this appearance
     */
    public BrainAppearance(
            final Color color, final boolean fill, final double offsetx, final double offsety)
    {
        this.color = color;
        this.fill = fill;
        this.offsetx = offsetx;
        this.offsety = offsety;
    }

    public abstract void draw(Screen screen, double x, double y, double zoom);

    public abstract double getWidth();

    public abstract double getHeight();
}
