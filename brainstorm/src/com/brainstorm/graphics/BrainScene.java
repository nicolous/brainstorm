package com.brainstorm.graphics;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Map.Entry;

import com.brainstorm.game.BrainGameEngine;
import com.brainstorm.unit.BrainObject;
import com.brainstorm.unit.Creature;
import com.symugine.game.GameObject;
import com.symugine.graphics.Appearance;
import com.symugine.graphics.Scene;
import com.symugine.utils.Location;
import com.symuscreen.screen.Screen;

/** Brainstorm scene */
public class BrainScene extends Scene
{
    /** The game screen */
    protected Screen screen;

    /** View bounds */
    protected Rectangle view;

    /**
     * New brainstorm scene
     * 
     * @param width
     *            The scene's width
     * @param height
     *            The scene's height
     * @param screen
     *            The game screen
     */
    public BrainScene(final int width, final int height, final Screen screen)
    {
        super(width, height);

        this.screen = screen;
        view = new Rectangle();
        selection = new ArrayList<GameObject>();
    }

    /**
     * Scroll the user view depending of the mouse's position
     * 
     * @param elapsed
     *            Time elapsed since the last render
     */
    private void scrollView(final double elapsed)
    {
        final Rectangle newView = new Rectangle(view);
        newView.width = screen.getWidth();
        newView.height = screen.getHeight();

        final Point pos = screen.getMousePosition();
        if (pos != null)
        {
            if (pos.x < 20)
            {
                newView.x -= elapsed / 5;
                if (newView.x < 0)
                {
                    newView.x = 0;
                }
            }
            else if (pos.x > screen.getWidth() - 20)
            {
                newView.x += elapsed / 5;
                if (newView.x + newView.width > width)
                {
                    newView.x = Math.max(width - newView.width, 0);
                }
            }
            if (pos.y < 20)
            {
                newView.y -= elapsed / 5;
                if (newView.y < 0)
                {
                    newView.y = 0;
                }
            }
            if (pos.y > screen.getHeight() - 20)
            {
                newView.y += elapsed / 5;
                if (newView.y + newView.height > height)
                {
                    newView.y = Math.max(height - newView.height, 0);
                }
            }
        }

        if (newView.x < view.x)
        {
            if (newView.y < view.y)
            {
                screen.setCursor(Cursor.NW_RESIZE_CURSOR);
            }
            else if (newView.y > view.y)
            {
                screen.setCursor(Cursor.SW_RESIZE_CURSOR);
            }
            else
            {
                screen.setCursor(Cursor.W_RESIZE_CURSOR);
            }
        }
        else if (newView.x > view.x)
        {
            if (newView.y < view.y)
            {
                screen.setCursor(Cursor.NE_RESIZE_CURSOR);
            }
            else if (newView.y > view.y)
            {
                screen.setCursor(Cursor.SE_RESIZE_CURSOR);
            }
            else
            {
                screen.setCursor(Cursor.E_RESIZE_CURSOR);
            }
        }
        else
        {
            if (newView.y < view.y)
            {
                screen.setCursor(Cursor.N_RESIZE_CURSOR);
            }
            else if (newView.y > view.y)
            {
                screen.setCursor(Cursor.S_RESIZE_CURSOR);
            }
            else
            {
                screen.setCursor(Cursor.DEFAULT_CURSOR);
            }
        }

        view = newView;
    }

    private void renderObjectView()
    {
        final Color background = screen.getBackground();
        final Color viewBackground =
            new Color(
                    background.getRed() + 5, background.getGreen() + 5,
                    background.getBlue() + 5);
        for (final GameObject obj : game.getObjects())
        {
            final BrainObject brainObj = (BrainObject) obj;
            final Location location = brainObj.getLocation();

            final int view = brainObj.getView();
            final int viewX = (int) (location.x - view);
            final int viewY = (int) (location.y - view);
            final int viewDameter = view * 2;
            screen.drawOval(
                    viewBackground, viewX - this.view.x, viewY - this.view.y, viewDameter,
                    viewDameter, true);
        }
    }

    private void renderObjectHealth()
    {
        for (final GameObject obj : game.getObjects())
        {
            final BrainObject brainObj = (BrainObject) obj;
            if (brainObj.getHealth() > 0)
            {
                final Location location = brainObj.getLocation();
                final double radius = brainObj.getRadius();

                final double maxBarLen = radius * 4;

                if (brainObj.getWeakening() > 0)
                {
                    final double healthRatio = brainObj.getHealth() / brainObj.getMaxHealth();
                    final double healthBarLen = healthRatio * maxBarLen;

                    Color healthBarColor = Color.RED;
                    if (healthRatio > 0.75)
                        healthBarColor = Color.GREEN.darker();
                    else if (healthRatio > 0.5)
                        healthBarColor = Color.GREEN;
                    else if (healthRatio > 0.25)
                        healthBarColor = Color.ORANGE;

                    final int barX = (int) (location.x - radius - maxBarLen / 4 + 1);
                    final int barY = (int) (location.y + radius * 2 - 2);
                    screen.drawRect(
                            healthBarColor, barX, barY, (int) Math.ceil(healthBarLen), 2, true);
                }

                final long lifeSpan = brainObj.getLifeSpan();
                if (lifeSpan != 0 && brainObj instanceof Creature)
                {
                    final double age = brainObj.getAge();
                    final double ageRatio = 1 - age / lifeSpan;
                    final double ageBarLen = ageRatio * maxBarLen;

                    Color ageBarColor = new Color(220, 220, 220);
                    if (age < 60 * 1000)
                    {
                        final int colorInd = (int) (ageRatio * 255);
                        ageBarColor = new Color(colorInd, colorInd, colorInd);
                    }
                    // ageBarColor = new Color(205, 205, 205);
                    // else if (ageRatio > 0.5)
                    // ageBarColor = new Color(155, 155, 155);
                    // else if (ageRatio > 0.25)
                    // ageBarColor = new Color(100, 100, 100);

                    final int barX = (int) (location.x - radius - maxBarLen / 4 + 1);
                    final int barY = (int) (location.y + radius * 2);
                    screen.drawRect(ageBarColor, barX, barY, (int) Math.ceil(ageBarLen), 2, true);
                }
            }
        }
    }

    private void renderObjects()
    {
        for (final GraphicsData data : graphicsData)
        {
            for (final Appearance appearance : data.appearances)
            {
                final BrainAppearance brainAppearance = (BrainAppearance) appearance;

                final int objX = (int) (data.x - brainAppearance.offsetx);
                final int objY = (int) (data.y - brainAppearance.offsety);
                final int objWidth = (int) brainAppearance.getWidth();

                if (new Rectangle(objX, objY, objWidth, objWidth).intersects(view))
                {
                    brainAppearance.draw(screen, objX - view.x, objY - view.y, 1);
                }
            }
        }
    }

    private void renderMinimap()
    {
        if (width > view.width || height > view.height)
        {
            final int miniFactor = 8;
            final int miniWidth = width / miniFactor;
            final int miniHeight = height / miniFactor;
            final int miniX = screen.getWidth() - miniWidth;
            final int miniY = screen.getHeight() - miniHeight;
            screen.clearRect(miniX, miniY, miniWidth, miniHeight);

            for (final GraphicsData data : graphicsData)
            {
                for (final Appearance appearance : data.appearances)
                {
                    final BrainAppearance brainAppearance = (BrainAppearance) appearance;
                    final int objX = miniX + (int) (data.x - brainAppearance.offsetx) / miniFactor;
                    final int objY = miniY + (int) (data.y - brainAppearance.offsety) / miniFactor;
                    brainAppearance.draw(screen, objX, objY, 1.0 / miniFactor);
                }
            }

            screen
            .drawRect(
                    Color.BLACK, miniX - 1, miniY - 1, miniWidth + 1, miniHeight + 1, false);
            screen.drawRect(Color.GRAY, miniX + view.x / miniFactor - 1, miniY
                    + view.y / miniFactor - 1, view.width / miniFactor + 1, view.height
                    / miniFactor + 1, false);
        }
    }

    private void renderSelection()
    {
        for (final GameObject object : selection)
        {
            if (object instanceof Creature)
            {
                final Creature creature = (Creature) object;
                final Location location = creature.getLocation();
                final double radius = creature.getRadius();

                final int size = (int) (radius * 5);
                final int selectionX = (int) (location.x - size / 2);
                final int selectionY = (int) (location.y - size / 2);
                screen.drawRect(Color.BLACK, selectionX, selectionY, size, size, false);
            }
        }
    }

    private void drawStats()
    {
        final BrainGameEngine gameEngine = (BrainGameEngine) game.getGameEngine();
        int y = screen.getHeight() - 2;
        for (final Entry<String, Integer> gentle : gameEngine.getPopulationStats().entrySet())
        {
            screen.drawString(Color.BLACK, gentle.getKey() + ": " + gentle.getValue(), 1, y);
            y -= 11;
            // System.out.print(gentle.getKey().getSimpleName() + ": " +
            // gentle.getValue() + " ");
        }

        screen.drawString(Color.BLACK, game.getCurrentFramerate() + " fps", 1, 10);

        final long thinkingTime = game.getCurrentThinkingTime();
        final long renderingTime = game.getCurrentRenderingTime();
        final long networkingTime = game.getCurrentNetworkingTime();
        final long soundTime = game.getCurrentSoundTime();
        final long totalTime = thinkingTime + renderingTime + networkingTime + soundTime;

        final String thinkingStats =
            String.format("%-13s %.2f%%", "Thinking:", (double) thinkingTime / totalTime * 100);
        final String renderingStats =
            String.format("%-13s %.2f%%", "Rendering:", (double) renderingTime
                    / totalTime * 100);
        final String networkingStats =
            String.format("%-13s %.2f%%", "Networking:", (double) networkingTime
                    / totalTime * 100);
        final String soundStats =
            String.format("%-13s %.2f%%", "Sound:", (double) soundTime / totalTime * 100);
        final int x = screen.getWidth() - 135;
        screen.drawString(Color.BLACK, thinkingStats, x, 10);
        screen.drawString(Color.BLACK, renderingStats, x, 21);
        screen.drawString(Color.BLACK, networkingStats, x, 32);
        screen.drawString(Color.BLACK, soundStats, x, 43);
    }

    @Override
    public void render(final double elapsed)
    {
        scrollView(elapsed);

        if (!graphicsData.isEmpty())
        {
            renderObjectView();
            renderObjectHealth();
            renderObjects();
            renderSelection();
            renderMinimap();
        }
        drawStats();

        screen.update();
    }
}
