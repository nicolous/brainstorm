Lancer une partie "Proies / Prédateurs" à partir de l'appli Java :
	java -jar predators.jar <jar-ia-proies> <jar-ia-prédateurs> [--tv port [--screen]]
Exemple :
	java -jar predators.jar mypreys.jar mypredators.jar
Exemple en diffusant la partie sur Internet :
	java -jar predators.jar mypreys.jar mypredators.jar --tv 12345 --screen
	
Lancer une partie "Proies / Prédateurs" à partir d'un script Jython :
	jython predators.py 
...ou lancer predators.py avec jython
Le nom du module et des classes Python sont écrits en dur dans le script. Le modifier au besoin.

Rappel : http://www.cssmatch.com/temp/brainstorm/javadoc/
