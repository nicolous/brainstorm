package com.symuscreen.screen;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Transparency;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 * Game screen. It's double-buffered, it supports fullscreen, and it supports different resolutions.
 * Event listeners can be registered.
 */
public class Screen
{
    /** Frame instance */
    protected JFrame frame;

    /** Frame title */
    protected String title;

    /** Frame size */
    protected Dimension size;

    /** Render area */
    protected Canvas canvas;

    /** Screen resolution */
    protected Dimension resolution;

    /** Background color */
    protected Color background;

    /** Current back buffer */
    protected Graphics2D backBuffer;

    /** Is it in fullscreen mode? */
    protected boolean fullscreen;

    /** Device used for fullscreen */
    protected GraphicsDevice device;

    /** Mouse listeners */
    protected List<MouseListener> mouseListeners;

    /** Mouse motion listeners */
    protected List<MouseMotionListener> mouseMotionListeners;

    /** Mouse wheel listeners */
    protected List<MouseWheelListener> mouseWheelListeners;

    /** Key listeners */
    protected List<KeyListener> keyListeners;

    /** Initialize the frame used in windowed mode */
    protected void initFrame()
    {
        if (frame != null)
        {
            frame.dispose();
        }

        frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setIgnoreRepaint(true);
    }

    /** Get the component that the listeners will listen */
    protected Component getComponent()
    {
        Component component = null;

        if (fullscreen)
        {
            final Window window = device.getFullScreenWindow();
            if (window != null)
            {
                component = window;
            }
        }
        else
        {
            component = canvas;
        }

        return component;
    }

    /** Initialize the device to enter prepare the fullscreen mode */
    protected void initFullscreen()
    {
        boolean success = false;

        if (device.isFullScreenSupported())
        {
            DisplayMode displayMode = null;
            for (final DisplayMode mode : device.getDisplayModes())
            {
                System.out.println(mode.getWidth()
                        + "x" + mode.getHeight() + " " + mode.getBitDepth() + " "
                        + mode.getRefreshRate());
                if (mode.getWidth() == resolution.width && mode.getHeight() == resolution.height)
                {
                    if (displayMode == null)
                    {
                        displayMode = mode;
                    }
                }
            }

            if (displayMode != null)
            {
                System.out.println("> "
                        + displayMode.getWidth() + "x" + displayMode.getHeight() + " "
                        + displayMode.getBitDepth() + " " + displayMode.getRefreshRate());

                quitWindowed();
                initFrame();

                frame.setBackground(background);
                frame.setUndecorated(true);
                // frame.setResizable(false); // uncomment me and the desktop's
                // toolbars will appear

                device.setFullScreenWindow(frame);
                if (device.isDisplayChangeSupported())
                {
                    try
                    {
                        device.setDisplayMode(displayMode);

                        // fix for mac os x
                        // Rectangle bounds =
                        // GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
                        frame.setSize(displayMode.getWidth(), displayMode.getHeight());

                        // avoid potential deadlock in 1.4.1_02
                        try
                        {
                            EventQueue.invokeAndWait(new Runnable()
                            {
                                public void run()
                                {
                                    frame.createBufferStrategy(2);
                                }
                            });
                        }
                        catch (final InterruptedException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch (final InvocationTargetException ex)
                        {
                            ex.printStackTrace();
                        }

                        success = true;

                        connectListeners();
                    }
                    catch (final IllegalArgumentException ex)
                    {
                        ex.printStackTrace();
                    }

                }
            }
        }
        if (!success)
        {
            setFullscreen(false);
        }
    }

    /** Quit the fullscreen mode */
    protected void quitFullScreen()
    {
        disconnectListeners();

        final Window window = device.getFullScreenWindow();
        if (window != null)
        {
            window.dispose();
        }

        device.setFullScreenWindow(null);
    }

    /** Enter in windowed mode */
    protected void initWindowed()
    {
        quitFullScreen();
        initFrame();

        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        canvas = new Canvas();
        canvas.setBackground(background);
        frame.setUndecorated(false);
        canvas.setSize(size);
        frame.add(canvas);
        frame.pack();
        frame.setResizable(false);
        frame.setLocation(
                (screenSize.width - size.width) / 2,
                (screenSize.height - size.getSize().height) / 2);

        canvas.addComponentListener(new ComponentAdapter()
        {
            @Override
            public void componentResized(final ComponentEvent arg0)
            {
                // Windows fix
                retrieveBackBuffer();
            }
        });

        frame.setVisible(true);
        canvas.createBufferStrategy(2);

        connectListeners();
    }

    /** Quit the windowed mode */
    protected void quitWindowed()
    {
        disconnectListeners();

        canvas = null;
    }

    /** [Re]Connect the listeners */
    protected void connectListeners()
    {
        final Component component = getComponent();

        if (component != null)
        {
            for (final MouseListener listener : mouseListeners)
            {
                component.addMouseListener(listener);
            }
            for (final MouseMotionListener listener : mouseMotionListeners)
            {
                component.addMouseMotionListener(listener);
            }
            for (final MouseWheelListener listener : mouseWheelListeners)
            {
                component.addMouseWheelListener(listener);
            }
            for (final KeyListener listener : keyListeners)
            {
                component.addKeyListener(listener);
            }
        }
    }

    /** Disconnect the listeners */
    protected void disconnectListeners()
    {
        final Component component = getComponent();

        if (component != null)
        {
            for (final MouseListener listener : mouseListeners)
            {
                component.removeMouseListener(listener);
            }
            for (final MouseMotionListener listener : mouseMotionListeners)
            {
                component.removeMouseMotionListener(listener);
            }
            for (final MouseWheelListener listener : mouseWheelListeners)
            {
                component.removeMouseWheelListener(listener);
            }
            for (final KeyListener listener : keyListeners)
            {
                component.removeKeyListener(listener);
            }
        }
    }

    /** Update the current back buffer reference memorized in this class */
    protected void retrieveBackBuffer()
    {
        final BufferStrategy strategy = getStrategy();
        if (strategy != null && !strategy.contentsLost())
        {
            backBuffer = (Graphics2D) strategy.getDrawGraphics();
        }
    }

    /**
     * Create a new screen
     * 
     * @param title
     *            The frame title
     * @param size
     *            The frame's size
     * @param resolution
     *            The resolution
     * @param fullscreen
     *            If the screen must enter in fullscreen mode
     * @param background
     *            Background color
     */
    public Screen(
            final String title, final Dimension size, final Dimension resolution,
            final boolean fullscreen, final Color background)
    {
        this.title = title;
        this.size = size;
        this.resolution = resolution;
        this.fullscreen = fullscreen;
        this.background = background;

        device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

        mouseListeners = new ArrayList<MouseListener>();
        mouseMotionListeners = new ArrayList<MouseMotionListener>();
        mouseWheelListeners = new ArrayList<MouseWheelListener>();
        keyListeners = new ArrayList<KeyListener>();

        initFrame();
        setFullscreen(fullscreen);
    }

    /** Add a mouse listener */
    public void addMouseListener(final MouseListener listener)
    {
        final Component component = getComponent();
        if (component != null)
        {
            component.addMouseListener(listener);
        }

        mouseListeners.add(listener);
    }

    /** Add a mouse motion listener */
    public void addMouseMotionListener(final MouseMotionListener listener)
    {
        final Component component = getComponent();
        if (component != null)
        {
            component.addMouseMotionListener(listener);
        }

        mouseMotionListeners.add(listener);
    }

    /** Add a mouse wheel listener */
    public void addMouseWheelListener(final MouseWheelListener listener)
    {
        final Component component = getComponent();
        if (component != null)
        {
            component.addMouseWheelListener(listener);
        }

        mouseWheelListeners.add(listener);
    }

    /** Add a key listener */
    public void addKeyListener(final KeyListener listener)
    {
        final Component component = getComponent();
        if (component != null)
        {
            component.addKeyListener(listener);
        }

        keyListeners.add(listener);
    }

    /** Get the mouse position on the canvas */
    public Point getMousePosition()
    {
        Point position = null;

        final Component component = getComponent();
        if (component != null)
        {
            position = component.getMousePosition();
        }

        return position;
    }

    /** Retrieve the current buffer strategy */
    protected BufferStrategy getStrategy()
    {
        BufferStrategy strategy = null;

        if (fullscreen)
        {
            final Window window = device.getFullScreenWindow();
            if (window != null)
            {
                strategy = window.getBufferStrategy();
            }
        }
        else
        {
            strategy = canvas.getBufferStrategy();
        }

        return strategy;
    }

    /** Enter/Quit the fullscreen mode */
    public void setFullscreen(final boolean fullscreen)
    {
        this.fullscreen = fullscreen;
        if (fullscreen)
        {
            initFullscreen();
        }
        else
        {
            initWindowed();
        }
        retrieveBackBuffer();
    }

    /** Swap the buffers and clear the back buffer */
    public void update()
    {
        backBuffer.dispose();

        final BufferStrategy strategy = getStrategy();
        if (strategy != null)
        {
            if (!strategy.contentsLost())
            {
                strategy.show();

                // (on Linux, this fixes event queue problems)
                // Toolkit.getDefaultToolkit().sync();

                retrieveBackBuffer();
                backBuffer.clearRect(0, 0, getWidth(), getHeight());
            }
        }
    }

    /** Get the screen width */
    public int getWidth()
    {
        int width = 0;

        if (fullscreen)
        {
            final Window window = device.getFullScreenWindow();
            if (window != null)
            {
                width = window.getWidth();
            }
        }
        else
        {
            width = canvas.getWidth(); // width = frame.getWidth();
        }

        return width;
    }

    /** Get the screen height */
    public int getHeight()
    {
        int height = 0;

        if (fullscreen)
        {
            final Window window = device.getFullScreenWindow();
            if (window != null)
            {
                height = window.getHeight();
            }
        }
        else
        {
            height = canvas.getHeight(); // height = frame.getHeight();
        }

        return height;
    }

    /** Get the screen resolution */
    public Dimension getResolution()
    {
        return resolution;
    }

    /** Get the screen background color */
    public Color getBackground()
    {
        return background;
    }

    /** Change the screen resolution */
    public void setResolution(final Dimension resolution)
    {
        this.resolution = resolution;
        setFullscreen(fullscreen);
    }

    /** Set the cursor to use on the canvas */
    public void setCursor(final int cursor)
    {
        final Component component = getComponent();
        if (component != null)
        {
            component.setCursor(new Cursor(cursor));
        }
    }

    /** Clear an area */
    public void clearRect(final int x, final int y, final int width, final int height)
    {
        backBuffer.clearRect(x, y, width, height);
    }

    /** Draw a string */
    public void drawString(final Color color, final String str, final int x, final int y)
    {
        backBuffer.setColor(color);
        backBuffer.drawString(str, x, y);
    }

    /** Draw a rectangle */
    public void drawRect(
            final Color color, final int x, final int y, final int width, final int height,
            final boolean fill)
    {
        backBuffer.setColor(color);
        if (fill)
        {
            backBuffer.fillRect(x, y, width, height);
        }
        else
        {
            backBuffer.drawRect(x, y, width, height);
        }
    }

    /** Draw an oval */
    public void drawOval(
            final Color color, final int x, final int y, final int width, final int height,
            final boolean fill)
    {
        backBuffer.setColor(color);
        if (fill)
        {
            backBuffer.fillOval(x, y, width, height);
        }
        else
        {
            backBuffer.drawOval(x, y, width, height);
        }
    }

    /**
     * Draw an image
     * 
     * @param image
     *            The image to draw
     * @param x
     *            The x-coordinate where to draw the image
     * @param y
     *            The y-coordinate where to draw the image
     * @param width
     *            The image width
     * @param height
     *            The image height
     */
    public void draw(final Image image, final int x, final int y, final int width, final int height)
    {
        backBuffer.drawImage(image, x, y, width/*
                                                * * getWidth() / resolution.width
                                                */, height/*
                                                           * * getHeight() / resolution.height
                                                           */, null);
    }

    /** Create an image from a file path */
    public static Image createImage(final String path) throws IOException
    {
        Image img = null;

        final BufferedImage buffer = ImageIO.read(new File(path));
        final GraphicsConfiguration config =
                GraphicsEnvironment
                        .getLocalGraphicsEnvironment().getDefaultScreenDevice()
                        .getDefaultConfiguration();
        img =
                config.createCompatibleImage(
                        buffer.getWidth(), buffer.getHeight(), Transparency.BITMASK);
        img.getGraphics().drawImage(buffer, 0, 0, null);

        return img;
    }

    /*
     * public static void main(String[] args) { new Screen(new Dimension(800, 600), new
     * Dimension(800, 600), false); }
     */
}
