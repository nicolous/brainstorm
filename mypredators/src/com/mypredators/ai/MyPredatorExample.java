package com.mypredators.ai;

import java.util.List;
import java.util.Random;

import com.brainstorm.ai.Actions;
import com.brainstorm.unit.ObjectInfo;
import com.predators.ai.Predator;
import com.predators.unit.PredatorCreature;
import com.symugine.utils.Location;
import com.symugine.utils.Movement;

public class MyPredatorExample extends Predator
{
    private final Random rand;

    public MyPredatorExample()
    {
        rand = new Random();
    }

    private void walk(final Actions actions, final ObjectInfo me)
    {
        final Movement movement = new Movement(me.getLocation(), me.getStamina());
        switch (rand.nextInt(4))
        {
        case 0:
            movement.location.x -= 100;
            break;
        case 1:
            movement.location.x += 100;
            break;
        case 2:
            movement.location.y -= 100;
            break;
        case 3:
            movement.location.y += 100;
            break;
        }
        actions.move(movement);
    }

    private boolean eat(final Actions actions, final ObjectInfo me, final List<ObjectInfo> objAround)
    {
        boolean success = false;

        ObjectInfo food = null;
        double foodDistance = Double.MAX_VALUE;
        ObjectInfo predator = null;
        double distance = 0;
        for (final ObjectInfo object : objAround)
        {
            if (me.isFood(object))
            {
                if (me.isInRange(object))
                {
                    if (me.getHealth() >= 50)
                    {
                        actions.reproduce();
                    }
                    else
                    {
                        actions.eat(object);
                    }
                    success = true;
                    break;
                }

                distance = me.distance(object);
                if (distance < foodDistance)
                {
                    food = object;
                    foodDistance = distance;
                }
            }
            else if (object.getObjectType() == PredatorCreature.class)
            {
                distance = me.distance(object);
                if (distance < foodDistance)
                {
                    predator = object;
                }
            }
        }

        if (!success)
        {
            if (food != null)
            {
                actions.move(new Movement(food.getLocation(), Math.min(
                        me.getStamina(), foodDistance)));
                success = true;
            }
            else if (predator != null)
            {
                final Location location = me.getLocation();
                final Location predatorLocation = predator.getLocation();
                if (location.x < predatorLocation.x)
                {
                    location.x -= 100;
                }
                else
                {
                    location.x += 100;
                }
                if (location.y < predatorLocation.y)
                {
                    location.y -= 100;
                }
                else
                {
                    location.y += 100;
                }
                actions.move(new Movement(location, me.getStamina()));
                success = true;
            }
        }

        return success;
    }

    @Override
    public Actions think(final double elapsed, final ObjectInfo me)
    {
        final Actions actions = new Actions();

        final List<ObjectInfo> objAround = me.lookAround();

        if (!eat(actions, me, objAround))
        {
            walk(actions, me);
        }

        return actions;
    }
}
