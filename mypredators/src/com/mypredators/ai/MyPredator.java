package com.mypredators.ai;

import java.util.List;
import java.util.Random;

import com.brainstorm.ai.Actions;
import com.brainstorm.unit.ObjectInfo;
import com.predators.ai.Predator;
import com.predators.unit.PredatorCreature;
import com.symugine.utils.Location;
import com.symugine.utils.Movement;

public class MyPredator extends Predator
{
    private final Random rand = new Random();

    private Location prevLoc;
    private long prevDirectionChange;
    private Actions prevActions;

    public MyPredator()
    {
        prevActions = new Actions();
        prevDirectionChange = Long.MAX_VALUE;
    }

    private void makeMovement(final Actions actions, final Movement movement, final ObjectInfo me)
    {
        prevLoc = me.getLocation();
        actions.move(movement);
    }

    private void walk(final double elapsed, final Actions actions, final ObjectInfo me)
    {
        prevDirectionChange += elapsed;

        final boolean moving = prevActions.movement != null;
        Movement movement;
        if (moving)
            movement = new Movement(prevActions.movement.location, me.getStamina());
        else
            movement = new Movement(me.getLocation(), me.getStamina());

        if (moving && me.getLocation().equals(prevLoc))
        {
            movement.location.x *= -1;
            movement.location.y *= -1;
        }
        else if (prevDirectionChange > 500)
        {
            switch (rand.nextInt(3))
            {
            case 0:
                movement.location.x -= 100;
                break;
            case 1:
                movement.location.x += 100;
                break;
            default:
                break;
            }
            switch (rand.nextInt(3))
            {
            case 0:
                movement.location.y -= 100;
                break;
            case 1:
                movement.location.y += 100;
                break;
            default:
                break;
            }
            prevDirectionChange = System.currentTimeMillis();
        }
        makeMovement(actions, movement, me);
    }

    private boolean eat(final Actions actions, final ObjectInfo me, final List<ObjectInfo> objAround)
    {
        boolean success = false;

        ObjectInfo food = null;
        double foodDistance = Double.MAX_VALUE;
        ObjectInfo predator = null;
        double distance = 0;
        for (final ObjectInfo object : objAround)
        {
            if (me.isFood(object))
            {
                if (me.isInRange(object))
                {
                    if (me.getHealth() >= 50)
                    {
                        actions.reproduce();
                    }
                    else
                    {
                        actions.eat(object);
                    }
                    success = true;
                    break;
                }

                distance = me.distance(object);
                if (distance < foodDistance)
                {
                    food = object;
                    foodDistance = distance;
                }
            }
            else if (object.getObjectType() == PredatorCreature.class)
            {
                distance = me.distance(object);
                if (distance < foodDistance)
                {
                    predator = object;
                }
            }
        }

        if (!success)
        {
            if (food != null)
            {
                makeMovement(actions, new Movement(food.getLocation(), Math.min(
                        me.getStamina(), foodDistance)), me);
                success = true;
            }
            else if (predator != null)
            {
                final Location location = me.getLocation();
                final Location predatorLocation = predator.getLocation();
                if (location.x < predatorLocation.x)
                {
                    location.x -= rand.nextInt(100);
                }
                else
                {
                    location.x += rand.nextInt(100);
                }
                if (location.y < predatorLocation.y)
                {
                    location.y -= rand.nextInt(100);
                }
                else
                {
                    location.y += rand.nextInt(100);
                }
                makeMovement(actions, new Movement(location, me.getStamina()), me);
                success = true;
            }
        }

        return success;
    }

    @Override
    public Actions think(final double elapsed, final ObjectInfo me)
    {
        final Actions actions = new Actions();

        final List<ObjectInfo> objAround = me.lookAround();

        if (!eat(actions, me, objAround))
        {
            walk(elapsed, actions, me);
        }

        prevActions = actions;
        return actions;
    }
}
