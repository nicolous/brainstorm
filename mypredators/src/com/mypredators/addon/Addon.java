package com.mypredators.addon;

import com.brainstorm.ai.Intelligence;
import com.brainstorm.ai.IntelligenceFactory;
import com.mypredators.ai.MyPredator;

public class Addon implements IntelligenceFactory
{
    @Override
    public Intelligence newIntelligence()
    {
        return new MyPredator();
    }
}
