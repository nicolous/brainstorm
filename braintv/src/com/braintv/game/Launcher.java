package com.braintv.game;

import java.awt.Color;
import java.io.IOException;
import java.net.UnknownHostException;

import com.brainstorm.game.BrainGame;
import com.brainstorm.game.BrainGameEngine;
import com.brainstorm.graphics.BrainGraphicsEngine;
import com.brainstorm.graphics.BrainScene;
import com.brainstorm.network.BrainNetworkEngine;
import com.symugine.graphics.BaseGui;
import com.symugine.utils.Dimension;
import com.symuscreen.screen.Screen;

/** Braintv launcher */
public class Launcher
{
    /**
     * Connect to brainstorm tv
     * 
     * @param ip
     *            Brainstorm tv server ip
     * @param port
     *            Brainstorm tv server port
     * @param fps
     *            FPS max
     * @throws UnknownHostException
     *             If the ip address can't be resolved
     * @throws IOException
     *             If the connection is refused
     */
    public static void launch(final String ip, final int port, final int fps)
            throws UnknownHostException, IOException
    {
        final BrainNetworkEngine networkEngine = new BrainNetworkEngine();
        final Dimension sceneDim = networkEngine.setClientTv(ip, port);
        if (sceneDim != null)
        {
            final Screen screen =
                    new Screen(
                            "Brainstorm TV", new java.awt.Dimension(400, 400),
                            new java.awt.Dimension(800, 600), false, Color.WHITE);
            final BrainScene scene =
                    new BrainScene((int) sceneDim.width, (int) sceneDim.height, screen);
            final BrainGameEngine gameEngine = new BrainGameEngine(true);
            final BrainGraphicsEngine graphicsEngine = new BrainGraphicsEngine(screen);
            final BaseGui gui = new BaseGui();

            final BrainGame game =
                    new BrainGame(screen, scene, gameEngine, networkEngine, graphicsEngine, gui);
            game.setFramerate(fps);
            game.run();
        }
    }

    /**
     * Connect to a brainstorm tv. Usage: java -jar braintv [--ip server-ip] [--port server-port]
     */
    public static void main(final String[] args)
    {
        String ip = "localhost";
        int port = 12345;
        int fps = 100;

        int iarg = 0;
        while (iarg < args.length)
        {
            if (args[iarg].equals("--ip"))
            {
                if (args.length - iarg > 1)
                {
                    ip = args[iarg + 1];
                    iarg += 1;
                }
                else
                {
                    System.err.println("Usage is --ip address");
                }
                iarg += 1;
            }
            else if (args[iarg].equals("--port"))
            {
                if (args.length - iarg > 1)
                {
                    try
                    {
                        port = Integer.parseInt(args[iarg + 1]);
                    }
                    catch (final NumberFormatException e)
                    {
                        System.err.println("Bad port number " + args[iarg + 1]);
                    }
                    iarg += 1;
                }
                else
                {
                    System.err.println("Usage is --port port");
                }
                iarg += 1;
            }
            else if (args[iarg].equals("--fps"))
            {
                if (args.length - iarg > 1)
                {
                    try
                    {
                        fps = Integer.parseInt(args[iarg + 1]);
                    }
                    catch (final NumberFormatException e)
                    {
                        System.err.println("Bad fps rate " + args[iarg + 1]);
                    }
                    iarg += 1;
                }
                else
                {
                    System.err.println("Usage is --fps maxfps");
                }
                iarg += 1;
            }
            else
            {
                System.err.println("Unknown option " + args[iarg]);
                iarg += 1;
            }
        }

        System.out.println("Connection to " + ip + ":" + port);
        try
        {
            launch(ip, port, fps);
        }
        catch (final UnknownHostException e)
        {
            System.err.println("Can't connect to "
                    + ip + ", please choose a different one with --ip");
        }
        catch (final IOException e)
        {
            System.err.println(e.getMessage());
        }
    }
}
