package com.symugine.game;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.symugine.graphics.BaseGraphicsEngine;
import com.symugine.graphics.BaseGui;
import com.symugine.graphics.Scene;
import com.symugine.network.BaseNetworkEngine;
import com.symugine.sound.BaseSoundEngine;

/**
 * Base game class. The game is in charge to manage the players and all the objects the game
 * involves. It also updates all the engine by calling their frame() method
 */
public class Game
{
    /** The game engine */
    protected BaseGameEngine gameEngine;

    /** The network engine */
    protected BaseNetworkEngine networkEngine;

    /** The graphics engine */
    protected BaseGraphicsEngine graphicsEngine;

    /** The sound engine */
    protected BaseSoundEngine soundEngine;

    /** The game scene */
    protected Scene scene;

    /** Objects in game */
    private final List<GameObject> objects;

    /** Objects that have to be added next frame */
    private final List<GameObject> objectsToAdd;

    /** Objects that have to be removed next frame */
    private final List<GameObject> objectsToRemove;

    /** If this game is running */
    private boolean running;

    /** Theoric frame rate */
    private int framerate;

    /** Real frame rate */
    private int currentFramerate;

    /** Real thinking time */
    private long currentThinkingTime;

    /** Real networking time */
    private long currentNetworkingTime;

    /** Real rendering time */
    private long currentRenderingTime;

    /** Real sound time */
    private long currentSoundTime;

    /**
     * New game
     * 
     * @param gameEngine
     *            The game engine to use
     * @param networkEngine
     *            The network engine to use
     * @param graphicsEngine
     *            The graphics engine to use
     * @param soundEngine
     *            The sound engine to use
     * @param gui
     *            The gui to use
     * @param scene
     *            The game scene
     */
    public Game(
            final BaseGameEngine gameEngine, final BaseNetworkEngine networkEngine,
            final BaseGraphicsEngine graphicsEngine, final BaseSoundEngine soundEngine,
            final BaseGui gui, final Scene scene)
    {
        this.gameEngine = gameEngine;
        this.networkEngine = networkEngine;
        this.graphicsEngine = graphicsEngine;
        this.soundEngine = soundEngine;

        this.scene = scene;

        objects = new LinkedList<GameObject>();
        objectsToAdd = Collections.synchronizedList(new LinkedList<GameObject>());
        objectsToRemove = Collections.synchronizedList(new LinkedList<GameObject>());

        running = false;

        framerate = 100;
        currentFramerate = 0;

        gameEngine.attachGame(this);

        networkEngine.attachGame(this);

        graphicsEngine.attachGame(this);
        graphicsEngine.attachGui(gui);
        graphicsEngine.attachScene(scene);

        soundEngine.attachGame(this);

        // gui.attachGraphicsEngine(graphicsEngine);

        scene.attachGame(this);
    }

    /** Get the game engine */
    public BaseGameEngine getGameEngine()
    {
        return gameEngine;
    }

    /** Get the graphics engine */
    public BaseGraphicsEngine getGraphicsEngine()
    {
        return graphicsEngine;
    }

    /** Get the network engine */
    public BaseNetworkEngine getNetworkEngine()
    {
        return networkEngine;
    }

    /** Get the sound engine */
    public BaseSoundEngine getSoundEngine()
    {
        return soundEngine;
    }

    /** Get the game scene */
    public Scene getScene()
    {
        return scene;
    }

    /** Get the theoric frame rate */
    public int getFramerate()
    {
        return framerate;
    }

    /** Set the theoric frame rate */
    public void setFramerate(final int framerate)
    {
        this.framerate = framerate;
    }

    /** Get the real frame rate */
    public int getCurrentFramerate()
    {
        return currentFramerate;
    }

    /** Get the real thinking time */
    public long getCurrentThinkingTime()
    {
        return currentThinkingTime;
    }

    /** Get the real networking time */
    public long getCurrentNetworkingTime()
    {
        return currentNetworkingTime;
    }

    /** Get the real rendering time */
    public long getCurrentRenderingTime()
    {
        return currentRenderingTime;
    }

    /** Get the real sound time */
    public long getCurrentSoundTime()
    {
        return currentSoundTime;
    }

    /** Determine if the game is currently running */
    public boolean isRunning()
    {
        return running;
    }

    /** Start the game */
    public void run()
    {
        int realFramerate = 0;
        long currentTime = System.currentTimeMillis();
        long lastFrameTime = currentTime;

        long elapsed = 0;
        long totalElapsed = 0;

        /*
         * double extrapElapsed = 0; double extrap = 0;
         */
        /*
         * double totalExtrap = 0; double totalExtrapElapsed = 0;
         */

        running = true;
        while (running)
        {
            realFramerate++;

            currentTime = System.currentTimeMillis();
            elapsed = currentTime - lastFrameTime;
            lastFrameTime = currentTime;
            totalElapsed += elapsed;

            /*
             * extrap = (framerate / 1000.0) * elapsed; extrapElapsed = elapsed + extrap;
             */
            /*
             * totalExtrap += extrap; totalExtrapElapsed += extrapElapsed;
             */

            if (totalElapsed > 1000)
            {
                currentFramerate = realFramerate;
                // System.out.println("this frame : elapsed = " + elapsed +
                // ", extrapolation = " + extrapElapsed);
                // System.out.println("upon 1s    : real framerate = " +
                // realFramerate + " (extrapolated = " + totalExtrap + ")");
                // System.out.println("             real elapsed = " +
                // totalElapsed + " (extrapolated = " + totalExtrapElapsed +
                // ")");
                // System.out.println();

                realFramerate = 0;
                totalElapsed = 0;
                /*
                 * totalExtrap = 0; totalExtrapElapsed = 0;
                 */
            }
            long startTime = System.currentTimeMillis();
            objects.removeAll(objectsToRemove);
            objectsToRemove.clear();
            objects.addAll(objectsToAdd);
            objectsToAdd.clear();
            gameEngine.frame(elapsed); // extrapElapsed
            currentThinkingTime = System.currentTimeMillis() - startTime;

            startTime = System.currentTimeMillis();
            networkEngine.frame(elapsed);
            currentNetworkingTime = System.currentTimeMillis() - startTime;

            startTime = System.currentTimeMillis();
            graphicsEngine.frame(elapsed);
            currentRenderingTime = System.currentTimeMillis() - startTime;

            startTime = System.currentTimeMillis();
            soundEngine.frame(elapsed);
            currentSoundTime = System.currentTimeMillis() - startTime;

            // System.out.println((Runtime.getRuntime().totalMemory() -
            // Runtime.getRuntime().freeMemory()) / (1024 * 1024) + " MB");

            if (framerate > 0)
            {
                try
                {
                    Thread.sleep(1000 / framerate);
                }
                catch (final InterruptedException e)
                {
                    e.printStackTrace();
                    endGame();
                }
            }
        }
    }

    /** End the game, this is the last frame */
    public void endGame()
    {
        running = false;
    }

    /** Add an object in game */
    public void addObject(final GameObject object)
    {
        objectsToAdd.add(object);
    }

    /** Remove an object from the game */
    public void removeObject(final GameObject object)
    {
        objectsToRemove.add(object);
    }

    /** Get the objects in game */
    public List<GameObject> getObjects()
    {
        return objects;
    }

    /**
     * Find an object in game by id
     * 
     * @param id
     *            The object's id
     * @return The object found, or null
     */
    public GameObject findObject(final long id)
    {
        for (final GameObject o : objects)
        {
            if (o.getId() == id)
            {
                return o;
            }
        }
        return null;
    }
}
