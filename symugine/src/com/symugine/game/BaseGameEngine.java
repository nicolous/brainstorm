package com.symugine.game;

import com.symugine.engine.Engine;

/** Base game engine. */
public class BaseGameEngine extends Engine
{
    @Override
    public void frame(final double elapsed)
    {
    }
}
