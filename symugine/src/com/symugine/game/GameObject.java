package com.symugine.game;

import java.util.ArrayList;

import com.symugine.graphics.Animation;
import com.symugine.graphics.Appearance;
import com.symugine.graphics.Scene;
import com.symugine.utils.Dimension;
import com.symugine.utils.Location;

/** Base game object */
public abstract class GameObject implements Comparable<GameObject>
{
    /** The generated game object id */
    private static long nextId = 1;

    /** The game played */
    protected Game game;

    /** The object's id */
    private final long id;

    /** The object's location */
    private final Location location;

    /**
     * The object's dimension (at least used to maintain the object on the scene)
     */
    protected Dimension dimension;

    /** The object's animations (how this object looks like at the time) */
    protected ArrayList<Animation> animations;

    /**
     * New game object
     * 
     * @param game
     *            The game that uses the object
     * @param location
     *            The object's location
     * @param dimension
     *            The object's dimension
     */
    public GameObject(final Game game, final Location location, final Dimension dimension)
    {
        id = nextId++;

        this.game = game;

        this.dimension = dimension;
        this.location = new Location(dimension.width / 2, dimension.height / 2);
        setLocation(location);

        animations = new ArrayList<Animation>();
    }

    @SuppressWarnings("unchecked")
    public GameObject(final GameObject o)
    {
        this(o.game, o.location, o.dimension);
        animations = (ArrayList<Animation>) o.animations.clone();
    }

    /** Get the object's id */
    public long getId()
    {
        return id;
    }

    /** Get the object's location */
    public Location getLocation()
    {
        return new Location(location.x, location.y);
    }

    /**
     * Set the object's location
     * 
     * @param location
     *            The new location
     */
    public void setLocation(final Location location)
    {
        final Scene scene = game.getScene();
        final int sceneWidth = scene.getWidth();
        final int sceneHeight = scene.getHeight();

        double destX = location.x;
        double destY = location.y;

        if (destX < 0)
        {
            destX = this.location.x;
        }
        else if (destX > sceneWidth)
        {
            destX = sceneWidth;
        }
        if (destY < 0)
        {
            destY = this.location.y;
        }
        else if (destY > sceneHeight)
        {
            destY = sceneHeight - dimension.height / 2;
        }

        this.location.x = destX;
        this.location.y = destY;
    }

    /**
     * Get the object's appearance components at the moment (e.g.: a chassis, plus an energic
     * shield)
     */
    public ArrayList<Appearance> getAppearances()
    {
        final ArrayList<Appearance> appearances = new ArrayList<Appearance>(animations.size());
        for (final Animation animation : animations)
        {
            appearances.add(animation.getAppearance());
        }
        return appearances;
    }

    /** Add an object animation */
    public void addAnimation(final Animation animation)
    {
        animations.add(animation);
    }

    /**
     * Update the object animations based on the elapsed time since the last update
     */
    public void updateAnimations(final double elapsed)
    {
        for (final Animation animation : animations)
        {
            animation.update(elapsed);
        }
    }

    /** Called whe this object is allowed to "think" (e.g.: let the AI work) */
    public abstract void think(double elapsed);

    @Override
    public int compareTo(final GameObject o)
    {
        if (id < o.id)
        {
            return -1;
        }
        if (id == o.id)
        {
            return 0;
        }
        return 1;
    }
}
