package com.symugine.utils;

public class Location
{
    public double x;
    public double y;

    public Location(final double x, final double y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(final Object object)
    {
        boolean ok = false;
        if (object instanceof Location)
        {
            final Location other = (Location) object;
            ok = x == other.x && y == other.y;
        }
        return ok;
    }
}
