package com.symugine.utils;

/** Movement description */
public class Movement
{
    /** The location to reach */
    public Location location;

    /** The movement's speed */
    public double speed;

    /**
     * New movement
     * 
     * @param location
     *            The location to reach
     * @param speed
     *            The movement's speed
     */
    public Movement(final Location location, final double speed)
    {
        this.location = location;
        this.speed = speed;
    }
}
