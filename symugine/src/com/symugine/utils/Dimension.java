package com.symugine.utils;

public class Dimension
{
    public double width;
    public double height;

    public Dimension(final double width, final double height)
    {
        this.width = width;
        this.height = height;
    }
}
