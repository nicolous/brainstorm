package com.symugine.engine;

import java.util.Deque;
import java.util.LinkedList;

import com.symugine.game.Game;

/** Base engine class */
public abstract class Engine
{
    /** The game played */
    protected Game game;

    /** Event queue that this engine has to process */
    private final Deque<EngineEvent> eventsQueue;

    public Engine()
    {
        eventsQueue = new LinkedList<EngineEvent>();
    }

    /** Attach a game to this engine */
    public void attachGame(final Game game)
    {
        this.game = game;
    }

    /** Push an event to process */
    public synchronized void pushEvent(final EngineEvent event)
    {
        eventsQueue.add(event);
    }

    /** Cause this engine to process its pending events */
    protected synchronized void processEvents()
    {
        while (!eventsQueue.isEmpty())
        {
            processEvent(eventsQueue.pop());
        }
    }

    /** Called each frame by the Game object */
    public abstract void frame(double elapsed);

    /** Process an event */
    protected void processEvent(final EngineEvent event)
    {
    }
}
