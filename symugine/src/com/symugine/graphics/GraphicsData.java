package com.symugine.graphics;

import java.util.List;

public class GraphicsData
{
    public double x, y;
    public List<Appearance> appearances;

    public GraphicsData(final double x, final double y, final List<Appearance> appearances)
    {
        this.x = x;
        this.y = y;
        this.appearances = appearances;
    }
}
