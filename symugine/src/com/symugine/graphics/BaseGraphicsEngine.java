package com.symugine.graphics;

import com.symugine.engine.Engine;

/** Base graphics engine */
public class BaseGraphicsEngine extends Engine
{
    /** Is the engine passive? (do the render?) */
    protected boolean passive;

    /** The game scene */
    protected Scene scene;

    /** The GUI */
    protected BaseGui gui;

    /**
     * New graphics engine
     * 
     * @param passive
     *            Is the engine passive? (do the render?)
     */
    public BaseGraphicsEngine(final boolean passive)
    {
        this.passive = passive;
    }

    /** Attach a scene to this engine (called by the Game) */
    public void attachScene(final Scene scene)
    {
        this.scene = scene;
    }

    /** Attach a GUI to this engine (called by the Game) */
    public void attachGui(final BaseGui gui)
    {
        this.gui = gui;
    }

    @Override
    public void frame(final double elapsed)
    {
        if (!passive)
        {
            scene.render(elapsed);
            gui.render(elapsed);
        }
    }
}
