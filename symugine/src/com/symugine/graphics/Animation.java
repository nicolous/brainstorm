package com.symugine.graphics;

import java.util.ArrayList;
import java.util.List;

/** An animation : a set of "appearances" which change with time */
public class Animation
{
    /** Animation frame/appearance */
    private class Frame
    {
        /** Appearance */
        public Appearance appearance;

        /** Frame duration */
        public long duration;

        /**
         * New frame
         * 
         * @param appearance
         *            The appearance that corresponds to this frame
         * @param duration
         *            The duration
         */
        public Frame(final Appearance appearance, final long duration)
        {
            this.appearance = appearance;
            this.duration = duration;
        }
    }

    /** The frames/appearances that composes this animation */
    private final List<Frame> frames;

    /** The current frame number */
    private int currentFrame;

    /** The total elapsed time */
    private long time;

    /** The total animation duration */
    private long duration;

    /** New empty animation */
    public Animation()
    {
        frames = new ArrayList<Frame>();
        currentFrame = 0;
        time = 0;
        duration = 0;
    }

    /**
     * Add a frame/appearance to this animation
     * 
     * @param appearance
     *            The appearance
     * @param duration
     *            The frame duration
     */
    public void addFrame(final Appearance appearance, final long duration)
    {
        this.duration += duration;
        frames.add(new Frame(appearance, this.duration));
    }

    /** Reset the animation */
    public void reset()
    {
        currentFrame = 0;
        time = 0;
    }

    /**
     * Update the animation (change the appearance depending on the time elapsed)
     */
    public void update(final double elapsed)
    {
        time += elapsed;
        if (time > duration)
        {
            time %= duration;
            currentFrame = 0;
        }

        if (time > frames.get(currentFrame).duration)
        {
            if (currentFrame == frames.size())
            {
                currentFrame = 0;
            }
            else
            {
                do
                {
                    currentFrame++;
                }
                while (time > frames.get(currentFrame).duration);
            }
        }
    }

    /** Get the current appearance */
    public Appearance getAppearance()
    {
        return frames.get(currentFrame).appearance;
    }
}
