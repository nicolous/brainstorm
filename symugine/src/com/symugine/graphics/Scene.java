package com.symugine.graphics;

import java.util.ArrayList;
import java.util.List;

import com.symugine.game.Game;
import com.symugine.game.GameObject;

public abstract class Scene
{
    /**
     * Graphics data that fills the scene's list of the elements (e.g.: game objects) to render
     */
    public static class GraphicsData
    {
        /** x-coordinate of the element */
        public double x;

        /** y-coordinate of the element */
        public double y;

        /** The corresponding appearance list */
        public List<Appearance> appearances;

        /**
         * New graphics data
         * 
         * @param x
         *            x-coordinate of the element
         * @param y
         *            y-coordinate of the element
         * @param appearances
         *            The corresponding appearance list
         */
        public GraphicsData(final double x, final double y, final ArrayList<Appearance> appearances)
        {
            this.x = x;
            this.y = y;
            this.appearances = appearances;
        }
    }

    /** The game played */
    protected Game game;

    /** The scene's width */
    protected int width;

    /** The scene's height */
    protected int height;

    /** The graphics data list to render */
    protected List<GraphicsData> graphicsData;

    /** Current selection. */
    protected List<GameObject> selection;

    /**
     * Add an object to the current selection.
     * 
     * @param object
     *            The object to select.
     */
    public void addSelection(final GameObject object)
    {
        selection.add(object);
    }

    /**
     * Remove an object from the current selection.
     * 
     * @param object
     *            The object to unselected.
     */
    public void removeSelection(final GameObject object)
    {
        selection.remove(object);
    }

    /**
     * Select objects.
     * 
     * @param selection
     *            The objects to select.
     */
    public void setSelection(final List<GameObject> selection)
    {
        this.selection = selection;
    }

    /**
     * Get the selected objects.
     * 
     * @return The selected objects.
     */
    public List<GameObject> getSelection()
    {
        return selection;
    }

    /**
     * New scene
     * 
     * @param width
     *            The scene's width
     * @param height
     *            The scene's height
     */
    public Scene(final int width, final int height)
    {
        this.width = width;
        this.height = height;
        graphicsData = new ArrayList<GraphicsData>();
    }

    /** Attach a game to this scene (Called by the game itself) */
    public void attachGame(final Game game)
    {
        this.game = game;
    }

    /** Get the scene's width */
    public int getWidth()
    {
        return width;
    }

    /** Get the scene's height */
    public int getHeight()
    {
        return height;
    }

    /**
     * Render the scene
     * 
     * @param elapsed
     *            Time elapsed since the last render
     */
    public abstract void render(double elapsed);

    /** Add graphics data to render next rendering */
    public void addGraphicsData(final GraphicsData toDisplay)
    {
        graphicsData.add(toDisplay);
    }

    /** Get the graphics data that are about to be rendered */
    public List<GraphicsData> getGraphicsData()
    {
        return graphicsData;
    }

    /** Empty the graphics data list to render */
    public void clearGraphicsData()
    {
        graphicsData.clear();
    }
}
