package com.symugine.network;

import com.symugine.engine.Engine;

/** Base network engine */
public class BaseNetworkEngine extends Engine
{
    @Override
    public void frame(final double elapsed)
    {
    }
}
