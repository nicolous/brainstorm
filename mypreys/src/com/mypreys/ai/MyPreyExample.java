package com.mypreys.ai;

import java.util.List;
import java.util.Random;

import com.brainstorm.ai.Actions;
import com.brainstorm.unit.ObjectInfo;
import com.predators.ai.Prey;
import com.symugine.utils.Movement;

public class MyPreyExample extends Prey
{
    private static Random rand = new Random();

    public boolean eat(final Actions actions, final ObjectInfo me, final List<ObjectInfo> objAround)
    {
        boolean success = false;

        for (final ObjectInfo obj : objAround)
        {
            if (me.isFood(obj))
            {
                if (me.isInRange(obj))
                {
                    actions.eat(obj);
                }
                else
                {
                    actions.move(new Movement(obj.getLocation(), me.getStamina()));
                }
                success = true;
                break;
            }
        }

        return success;
    }

    public void walk(final Actions actions, final ObjectInfo me)
    {
        final Movement movement = new Movement(me.getLocation(), me.getStamina());

        switch (rand.nextInt(4))
        {
        case 0:
            movement.location.x += 100;
            break;
        case 1:
            movement.location.x -= 100;
            break;
        case 2:
            movement.location.y += 100;
            break;
        case 3:
            movement.location.y -= 100;
            break;
        }

        actions.move(movement);
    }

    @Override
    public Actions think(final double elapsed, final ObjectInfo me)
    {
        final Actions actions = new Actions();

        if (!eat(actions, me, me.lookAround()))
        {
            walk(actions, me);
        }

        return actions;
    }
}
