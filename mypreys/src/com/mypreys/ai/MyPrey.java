package com.mypreys.ai;

import java.util.List;
import java.util.Random;

import com.brainstorm.ai.Actions;
import com.brainstorm.unit.ObjectInfo;
import com.brainstorm.unit.PheromoneInfo;
import com.predators.ai.Prey;
import com.predators.unit.PredatorCreature;
import com.predators.unit.PreyCreature;
import com.symugine.utils.Location;
import com.symugine.utils.Movement;

public class MyPrey extends Prey
{
    private static final Random rand = new Random();
    private static final long PHEROMONE_FOOD = rand.nextLong();
    private static final long PHEROMONE_INCOMING = PHEROMONE_FOOD + rand.nextLong();

    private Location prevLoc;
    private long prevDirectionChange;
    private Actions prevActions;

    public MyPrey()
    {
        prevActions = new Actions();
        prevDirectionChange = Long.MAX_VALUE;
    }

    private void makeMovement(final Actions actions, final Movement movement, final ObjectInfo me)
    {
        prevLoc = me.getLocation();
        actions.move(movement);
    }

    private void walk(final double elapsed, final Actions actions, final ObjectInfo me)
    {
        prevDirectionChange += elapsed;

        final boolean moving = prevActions.movement != null;
        Movement movement;
        if (moving)
            movement = new Movement(prevActions.movement.location, me.getStamina());
        else
            movement = new Movement(me.getLocation(), me.getStamina());

        if (moving && me.getLocation().equals(prevLoc))
        {
            movement.location.x *= -1;
            movement.location.y *= -1;
        }
        else if (prevDirectionChange > 500)
        {
            switch (rand.nextInt(3))
            {
            case 0:
                movement.location.x -= 100;
                break;
            case 1:
                movement.location.x += 100;
                break;
            default:
                break;
            }
            switch (rand.nextInt(3))
            {
            case 0:
                movement.location.y -= 100;
                break;
            case 1:
                movement.location.y += 100;
                break;
            default:
                break;
            }
            prevDirectionChange = System.currentTimeMillis();
        }
        makeMovement(actions, movement, me);
    }

    private boolean eat(final Actions actions, final ObjectInfo me, final List<ObjectInfo> objAround)
    {
        boolean success = false;

        final double healthRatio = me.getHealth() / me.getMaxHealth();
        ObjectInfo foodInRange = null;
        ObjectInfo food = null;
        ObjectInfo foodPheromone = null;
        ObjectInfo incomingPheromone = null;
        ObjectInfo predator = null;
        double foodDistance = Double.MAX_VALUE;
        double foodPhDistance = -1;
        double incomingDistance = Double.MAX_VALUE;
        double predatorDistance = Double.MAX_VALUE;
        double distance = 0;
        int preyCount = 0;
        int foodCount = 0;
        for (final ObjectInfo object : objAround)
        {
            if (me.isFood(object))
            {
                if (me.isInRange(object))
                {
                    foodInRange = object;
                }
                else
                {
                    distance = me.distance(object);
                    if (distance < foodDistance)
                    {
                        food = object;
                        foodDistance = distance;
                    }
                }

                foodCount++;
            }
            else if (object.isPheromone())
            {
                final PheromoneInfo phInfo = me.getPheromone(object);
                if (phInfo.data == PHEROMONE_FOOD)
                {
                    distance = me.distance(object);
                    if (distance > foodPhDistance)
                    {
                        foodPheromone = object;
                        foodPhDistance = distance;
                    }
                }
                else if (phInfo.data == PHEROMONE_INCOMING)
                {
                    distance = me.distance(object);
                    if (distance < incomingDistance)
                    {
                        incomingPheromone = object;
                        incomingDistance = distance;
                    }
                }
            }
            else if (object.getObjectType() == PredatorCreature.class)
            {
                distance = me.distance(object);
                if (distance < predatorDistance)
                {
                    predator = object;
                    predatorDistance = distance;
                }
            }
            else if (object.getObjectType() == PreyCreature.class)
            {
                preyCount++;
            }
        }

        if (incomingPheromone != null)
        {
            if (predator == null && healthRatio > 0.5)
            {
                actions.reproduce();
            }
            else
            {
                final Location location = me.getLocation();
                final Location incomingLocation = incomingPheromone.getLocation();
                if (location.x < incomingLocation.x)
                {
                    location.x -= 100;
                }
                else
                {
                    location.x += 100;
                }
                if (location.y < incomingLocation.y)
                {
                    location.y -= 100;
                }
                else
                {
                    location.y += 100;
                }
                makeMovement(actions, new Movement(location, me.getStamina()), me);
            }
            success = true;
        }
        else if (predator != null)
        {
            actions.dropPheromone(new PheromoneInfo(200, 500, PHEROMONE_INCOMING));
            success = true;
        }

        else if (foodInRange != null)
        {
            if (healthRatio < 0.80)
            {
                actions.eat(foodInRange);
            }
            success = true;
        }
        else if (food != null)
        {
            if (foodPheromone == null)
                actions.dropPheromone(new PheromoneInfo(1, 500, PHEROMONE_FOOD));
            else
                makeMovement(actions, new Movement(food.getLocation(), Math.min(me
                        .getStamina(), foodDistance)), me);
            success = true;
        }
        else if (foodPheromone != null)
        {
            makeMovement(actions, new Movement(foodPheromone.getLocation(), Math.min(me
                    .getStamina(), foodPhDistance)), me);
            success = true;
        }
        else if (healthRatio > 0.5)
        {
            actions.reproduce();
            success = true;
        }

        return success;
    }

    @Override
    public Actions think(final double elapsed, final ObjectInfo me)
    {
        final Actions actions = new Actions();

        final List<ObjectInfo> objAround = me.lookAround();

        if (!eat(actions, me, objAround))
        {
            walk(elapsed, actions, me);
        }

        prevActions = actions;
        return actions;
    }
}
