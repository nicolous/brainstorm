package com.mypreys.addon;

import com.brainstorm.ai.Intelligence;
import com.brainstorm.ai.IntelligenceFactory;
import com.mypreys.ai.MyPrey;

public class Addon implements IntelligenceFactory
{
    @Override
    public Intelligence newIntelligence()
    {
        return new MyPrey();
    }
}
