#coding:Utf-8
"""Exemple d'addon qui définit une IA pour proies
"""

import sys
sys.path.append("symugine.jar")
sys.path.append("brainstorm.jar")
sys.path.append("predators.jar")

from com.brainstorm.ai import IntelligenceFactory
from com.predators.ai import Prey
from com.brainstorm.ai import Actions
from com.symugine.utils import Movement

import random
	
# Générateur d'IA pour proies utilisé par le jeu
class Addon(IntelligenceFactory):
	def newIntelligence(self):
		return MyPrey()


# Le jeu : des proies doivent échapper à des prédateurs
# les proies doivent manger de l'herbe pour survivre
# les prédateurs doivent manger des proies

# Une IA pour les proies
class MyPrey(Prey):

	# Une fonction appelée dans ce code pour se nourrir
	def eat(self, actions, me, objAround):
		success = False
	
		# Parmi les objets en vue, s'il y a la nourriture on se dirige vers elle
		# Si elle est à portée, on la mange
		for obj in objAround:
			if me.isFood(obj):
				if me.isInRange(obj):
					actions.eat(obj)
				else:
					actions.move(Movement(obj.getLocation(), me.getStamina()))
				success = True
				break
	
		return success

	# Une fonction appelée dans ce code pour bouger la proie en l'absence de nourriture
	def walk(self, actions, me):
		movement = Movement(me.getLocation(), me.getStamina())
	
		alea = random.randint(0, 3)
		if alea == 0:
			movement.location.x += 100
		elif alea == 1:
			movement.location.x -= 100
		elif alea == 2:
			movement.location.y += 100
		elif alea == 3:
			movement.location.y -= 100

		actions.move(movement)

	# Une fonction appelée à intervalles réguliers par le moteur du jeu
	# "elapsed" correspond au temps écoulé depuis le dernier appel
	# "me" donne accès à différentes informations sur la proie que l'on dirige
	# La fonction retourne les actions que l'ia veut effectuer
	def think(self, elapsed, me):
		actions = Actions()
		
		# Si on ne mange pas, on bouge
		# "lookAround()" retourne une liste d'objets en vue
		if not self.eat(actions, me, me.lookAround()):
			self.walk(actions, me)

		return actions

