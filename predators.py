"""Lancer la partie.
Toute implementation plus "dynamique" de la chose est la bienvenue.
"""

import sys
sys.path.append("symuscreen.jar")
sys.path.append("predators.jar")
sys.path.append("mypredators.jar")

import mypreys
from com.mypredators.addon import Addon
from com.predators.game import Launcher

Launcher.launchGame(mypreys.Addon(), Addon(), 0, 400, 400)

