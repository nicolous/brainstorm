import java.awt.Color;
import java.awt.Dimension;

import com.brainstorm.network.BrainNetworkEngine;
import com.predators.game.Launcher;
import com.symuscreen.screen.Screen;

public class Predators
{
    private static final int SCREEN_WIDTH = 600;
    private static final int SCREEN_HEIGHT = 600;
    private static final boolean SCREEN_FULLSCREEN = false;
    private static final int RESOLUTION_WIDTH = 1920;
    private static final int RESOLUTION_HEIGHT = 1080;

    private static final int SCENE_WIDTH = 600;
    private static final int SCENE_HEIGHT = 600;

    private static final int FPS_MAX = 0;

    private static final int TV_PORT = 12345;

    public static void main(final String[] args) throws Exception
    {
        final BrainNetworkEngine.Mode networkMode = BrainNetworkEngine.Mode.NONE;

        Screen screen = null;
        screen =
                new Screen("Predators", new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT), new Dimension(
                        RESOLUTION_WIDTH, RESOLUTION_HEIGHT), SCREEN_FULLSCREEN, new Color(
                                250, 250, 250));
        switch (networkMode)
        {
        case NONE:
            // screen = new Screen("Predators", new Dimension(SCREEN_WIDTH,
            // SCREEN_HEIGHT), new Dimension(RESOLUTION_WIDTH,
            // RESOLUTION_HEIGHT), false, Color.WHITE);
            // screen.setFullscreen(true);
            Launcher.launchGame(
                    screen, new com.mypreys.addon.Addon(), new com.mypredators.addon.Addon(),
                    FPS_MAX, SCENE_WIDTH, SCENE_HEIGHT);
            break;
        case TV_SERVER:
            System.out.println("Server");
            Launcher.launchTv(
                    screen, new com.mypreys.addon.Addon(), new com.mypredators.addon.Addon(),
                    TV_PORT, FPS_MAX, SCENE_WIDTH, SCENE_HEIGHT);
            break;
        }
    }
}
