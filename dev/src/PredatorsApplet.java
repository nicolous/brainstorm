import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JApplet;

import com.predators.game.Launcher;
import com.symuscreen.screen.Screen;

public class PredatorsApplet extends JApplet
{
    private static final long serialVersionUID = -2753185167637105729L;

    private class PredatorsScreen extends Screen
    {
        public PredatorsScreen(final Dimension size, final Dimension resolution)
        {
            super("", size, resolution, false, Color.WHITE);
        }

        @Override
        protected void initWindowed()
        {
            canvas = new Canvas();
            canvas.setBackground(background);

            canvas.setSize(size);
            getContentPane().add(canvas);

            canvas.createBufferStrategy(2);

            // connectListeners();
        }
    }

    @Override
    public void init()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                final Dimension size = getSize();
                final PredatorsScreen screen = new PredatorsScreen(size, new Dimension(800, 600));
                Launcher.launchGame(
                        screen, new com.mypreys.addon.Addon(), new com.mypredators.addon.Addon(),
                        50, size.width, size.height);
            }
        }).start();
    }
}
